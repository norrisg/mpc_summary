# README - MPC SUMMARY

## TO-DO

- Add more shit that's useful on exam
- Add definition positive definiteness
- Add reminder/tips for quick vector calculus (i.e for gradient of lagrangian) -> prevent mistakes!

***

## CHANGELOG

### Changelog - 2023.02.20 `v1.3.0`

- **Updated for public consumption**
- Moved sections to own files
- Added email / git hyperlinks

### Changelog - 2022.08.10 `v1.2.6`

- Added tips/tricks/corrections for exam-ready version

### Changelog - 2022.08.09 `v1.2.5`

- Compactified `CONVEX OPTIMIZATION`
  - merged `NOMENCALTURE` into `PROBLEM FORMULATION`
  - moved `CONVEX SETS` one column earlier
- Used new space to add `MATRIX CALCULUS` helper stuff for `KKT`
- Minor tweaks to improve `TUBE MPC IMPLEMENTATION`
- Tweaks to `UNCONSTRAINED LQR CONTROL`

### Changelog - 2022.07.31 `v1.2.4`

- Tweaked `SOFT CONSTRAINTS`
- Minor formatting fixes to `REFERENCE TRACKING`

### Changelog - 2022.07.31 `v1.2.3`

- Compactified `CFTOC`
- Compacted and ergänzt `FEASIBILITY AND STABILITY`
  - Added useful properties from `EX07-P2`

### Changelog - 2022.07.29 `v1.2.1`

- Compactification throughout early and mid chapters
- Added `ROBUST MPC III`
- Added `IMPLEMENTATION`

### Changelog - 2022.07.27 `v1.1.0`

- Reworked `SYSTEM THEORY`
- Reworked `UNCONSTRAINED LQR CONTROL`
  - notably `RECURSIVE APPROACH`, added `INFINITE HORIZON LQR`
- Completed and reworked `CONVEX OPTIMIZATION` 
  - notably `- SETS`, `- FUNCTIONS`, `OPTIMALITY CONDITIONS`, and `KKT`
  - Optimality conditions and KKT still probably need some work

### Changelog - 2022.07.26 `v1.0.0`

- `norrisgsummary.cls` -> updated from `RECURSIVE ESTIMATION` repo

- Reworked `CFTOC`
- Finished `INVARIANCE`
- Added `FEASIBILITY AND STABILITY`
- Added `PRACTICAL MPC`
- Added `ROBUST MPC I`
- Added `ROBUST MPC II`

***

### Changelog - 2022.04.27

- `norrisgsummary.cls`
  - minor changes to `b/w` color option:
      \colorlet{color3}{black!15} -> \colorlet{color3}{black!20}
  - Added new `tcolorbox` environment: `highlightbox`
    - Intended to highlight mixed text/math as in theorems or statements
  - Modified existing `yellowbox` and `bluebox` to contain several qol updates

***

### Changelog - 2022.04.25

- Continued work on CFTOC
  - Wrote out inequality constraints for `CONSTRUCTION WITHOUT SUBSTITUTION` 
- Laid framework for remainder of `Chapter 4: CFTOC`
- please make the suffering of this chapter stop...

***

### Changelog - 2022.03.31

- Added Section `CFTOC` -- If someone else wants to finish this please, please do.
  - Began subsection `QUADRATIC COST CFTOC`
    - `Construction With Substitution` needs to be compacted, seems repetitive and unnecessarily long
    - `Construction w/o Substitution` has ginormous matrices -> maybe check `nicematrix` package to see if there is some better strategy
  
- Added Section `INVARIANCE`

### Changelog - 2022.03.23

- Small corrections and formatting changes

***

### Changelog - 2022.03.21

- Continued `CONVEX OPTIMIZATION`
  - Further work on duality conditions and KKT
  - duality gap needs clarification
  - Slater Condition needs clarification

***

### Changelog - 2022.03.21

- Continued `CONVEX OPTIMIZATION`
  - wrote more of `CONVEX FUNCTIONS`
  - began `OPTIMALITY CONDITIONS/KKT`


***

### Changelog - 2022.03.15

- Began `CONVEX OPTIMIZATION` Section
  - `PROBLEM FORMULATION` seems a little overly detailed
  - `NOMENCLATURE` needs to be compactified
  - `CONVEX SETS` too many imamges?
  - `CONVEX FUNCTIONS` needs to be completed, image too small? image unnecessary?
  
***

### Changelog - 2022.03.11

- Began `UNCONSTRAINED LQR CONTROL`
  - Commented out `PROBLEM FORMULATION` (unnecessary/unused)
  - `RECURSIVE APPROACH` needs to be completed
- Figured out how to replicate sans-serif math fonts from lecture: `\usepackage[cmbright]{sfmath}`
- Maybe remove `INTRODUCTION` section to save space? Seems superfluous

***

### Changelog - 2022.03.07

- Finished discretization
- Wrote `linear System analysis`
- Began `nonlinear system analysis`

***

### Changelog - 2022.03.06

- Wrote initial `Introduction` section
- Began `System Theory` section
  - General NL model
  - Linearization
  - Discretization (incomplete)

***

### Changelog - 2022.03.02

- Initial commit
