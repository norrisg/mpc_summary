\section{CFTOC}

\begin{whitebox}{\textbf{CFTOC -- CONSTRAINED FINITE-TIME OPT. CONTROL}}


    \blue{Constrained Linear Optimal Control}
    \mathboxplus{
        J^\star (x(k)) = \textstyle\min_{U} \ &l_f(x_N) + \textstyle\sum_{i=0}^{N-1} l(x_{i}, u_{i}) \\
        \mathrm{subj. \ to \ }  &x_{i+1} = A x_{i} + Bu_{i} \\
        & x_{i} \in \mathcal{X}, \quad u_{i} \in \mathcal{U} \\
        & x_N \in \mathcal{X}_f, \quad x_0 = x(k)
    }

    \begin{itemize}[leftmargin=1em, itemsep = 2pt]
        \item \textbf{Quad. Cost / Squared Euclidian Norm:}
        \mathbox{
            J(x(k)) = x_N^\top P x_N + \textstyle\sum_{i=0}^{N-1}x_i^\top Q x_i + u_i R u_i
        }
        \item \textbf{p-Norm:} 
        $J(x(k)) = \lvert\lvert Px_N \rvert\rvert_p + \textstyle\sum_{i=0}^{N-1} \lvert\lvert Qx_i \rvert\rvert_p + \lvert\lvert Ru_i \rvert\rvert_p$
    \end{itemize}
    
    
\end{whitebox}

\begin{whitebox}{\textbf{TRANSFORM QUAD CFTOC TO QP}}

    \begin{minipage}[c]{0.4\linewidth}
        \blue{QP Problem} \\
        \textbf{Goal} -- Rewrite Quad. Cost CFTOC as QP \\
        $\leadsto$ easier to solve
    \end{minipage}
    \begin{minipage}[c]{0.55\linewidth}
        \mathboxplus{
            \min_{z\in\mathbb{R}^n} \ &\textstyle\frac{1}{2}z^\top H z + q^\top z + r \\
            \mathrm{subj. \ to \ } &Gz \leq h \\
            &Az = b
        }
    \end{minipage}
    
    \tcbsubtitle{\textbf{CONSTRUCTION WITH SUBSTITUTION}}
    \blue{IDEA} -- Sub. state eqns $x_{i+1} = Ax_i + Bu_i, \quad x_0 = x(k)$ \\
    \blue{Cost} -- Rewrite as
    \mathbox{
        J^\star(x(k)) = \min_U
        &\begin{bmatrix}
            U^\top & x(k)^\top
        \end{bmatrix}
        \left[\begin{smallmatrix}
            H & F^\top \\
            F & Y 
        \end{smallmatrix}\right]
        \begin{bmatrix}
            U^\top & x(k)^\top
        \end{bmatrix}^\top \\
        \mathrm{subj. \ to \ } &GU \leq w + Ex(k)
    }
    \blue{Constraints} -- Rewrite as $GU \leq w + Ex(k)$
    \mathbox{
        \mathcal{X} \!=\! \{x | A_x x \!\leq\! b_x\}
        \quad \mathcal{U} \!=\! \{u | A_u u \!\leq\! b_u\} 
        \quad \mathcal{X}_f \!=\! \{ x | A_f x \!\leq\! b_f\}
    }
    \mathbox{
        G = 
        \left[
            \begin{smallmatrix}
                A_u & & \\
                & \cdots & \\
                & & A_u \\
                \midrule
                0 & \cdots & 0 \\
                A_xB & & \\
                A_x A B & A_x B & 0\\
                \midrule
                A_f A^{N-1}B & \cdots & A_f B
            \end{smallmatrix}
        \right],
        E = 
        \left[
            \begin{smallmatrix}
                0 \\
                \cdots \\
                0 \\
                \midrule
                -A_x \\
                -A_xA \\
                \cdots \\
                \midrule
                -A_f A^N
            \end{smallmatrix}
        \right],
        w =
        \left[
            \begin{smallmatrix}
                b_u \\
                \cdots \\
                b_u \\
                \midrule
                b_x \\
                b_x \\
                \cdots \\
                \midrule
                b_f
            \end{smallmatrix}
        \right]
    }
    \blue{Solution} For a given $x(k)$, $U^\star$ can be found via QP solver

    \tcbsubtitle{\textbf{CONSTRUCTION WITHOUT SUBSTITUTION}}
    \blue{Idea} -- Keep state eqns as eq. constraints \\
    \blue{Cost} with $z = 
    \begin{bmatrix}
        x_1^\top & \dots x_N^\top & u_0^\top & \dots u_{N-1}^\top
    \end{bmatrix}^\top$
    \mathbox{
        J^\star(x(k)) = \min_z
        &\left[
            z^\top \ x(k)^\top
        \right]
        \left[\begin{smallmatrix}
            \bar{H} & 0 \\
            0 & Q 
        \end{smallmatrix}\right]
        \left[
            z^\top \ x(k)^\top
        \right]^\top \\
        \mathrm{subj. \ to \ } &G_{in}z \leq w_{in} + E_{in}x(k) \\
        &G_{eq}z = E_{eq}x(k)
    }
    \mathbox{
        \bar{H} = \mathrm{diag}(Q, \dots, Q, P, R, \dots, R)
    }
    \blue{Equality Constraints} from System Dyn. $x_{i+1} = Ax_i + Bu_i$
    \mathbox{
        G_{eq} = 
        \left[
        \begin{smallmatrix}
            \mathbb{I} & & \\
            -A & \mathbb{I} & \\
            & -A & \mathbb{I} \\
            &  \cdots & \cdots
        \end{smallmatrix}
        \middle|
        \begin{smallmatrix}
            -B & & \\
            & -B & \\
            & & -B \\
            & & \cdots
        \end{smallmatrix}
        \right],
        E_{eq} = 
        \left[
        \begin{smallmatrix}
            A \\
            0 \\
            \cdots \\
            0
        \end{smallmatrix}
        \right]
    }
    \blue{Inequality Constraints}
    \mathbox{
        \mathcal{X} \!=\! \{x | A_x x \!\leq\! b_x\}
        \quad \mathcal{U} \!=\! \{u | A_u u \!\leq\! b_u\} 
        \quad \mathcal{X}_f \!=\! \{ x | A_f x \!\leq\! b_f\}
    }
    \mathbox{
        G_{in} = 
        \left[
        \begin{smallmatrix}
            0 & & & \\
            \midrule
            A_x & & & \\
            & A_x & & \\
            & & \cdots & \\
            & & & A_f \\
            \midrule
            0 & & & \\
            & 0 & & \\
            & & \cdots & \\
            & & & 0
        \end{smallmatrix}
        \middle|
        \begin{smallmatrix}
            0 & & & \\
            \midrule
            0 & & & \\
            & 0 & & \\
            & & \cdots & \\
            & & & 0 \\
            \midrule
            A_u & & & \\
            & A_u & & \\
            & & \cdots & \\
            & & & A_u
        \end{smallmatrix}
        \right]\!\!,  %% Hacky fix, how to change?
        w_{in} = 
        \left[
        \begin{smallmatrix}
            b_x \\
            \midrule
            b_x \\
            b_x \\
            \cdots \\
            b_f \\
            \midrule
            b_u \\
            b_u \\
            \cdots \\
            b_u \\
        \end{smallmatrix}
        \right]\!\!,  %% Hacky fix, how to change?
        E_{in} = 
        \left[
        \begin{smallmatrix}
            -A_x^T \\
            0 \\
            \cdots \\
            0 \\
        \end{smallmatrix}
        \right]
    }
    

    \tcbsubtitle{\textbf{QP FEEDBACK SOLUTION}}
    \blue{From CFTOC problem as multiparametric QP}
    \mathboxplus{
        J^{\star}(x(k)) =\min_{U} &\left[U^{\top} x(k)^{\top}\right]
        \left[\begin{smallmatrix} H & F^{\top} \\ F & Y \end{smallmatrix}\right]
        \left[U^{\top} x(k)^{\top}\right]^{\top} \\
        \mathrm{subj.\ to}\ &G U \leq w+E x(k)
    }
    \blue{Solution Properties}
    \begin{itemize}[leftmargin=1em, itemsep = 2pt]
        \item  \textbf{First component of optimal solution:}
        \begin{align*}
            u_{0}^{\star}=\kappa(x(k)), \quad \forall x(k) \in \mathcal{X}_{0}
        \end{align*}
        $\kappa: \mathbb{R}^{n} \rightarrow \mathbb{R}^{m}$ is cont. and pw. affine on Polyhedra
        \begin{align*}
            \kappa(x)=F^{j} x+g^{j} \quad \text { if } \quad x \in C R^{j}, \quad j=1,\ldots,N^r
        \end{align*}
        \item  Polyhedral sets
            $C R^{j}=\left\{x \in \mathbb{R}^{n} \mid H^{j} x \leq K^{j}\right\}, j=1, \ldots, N^{r}$ \\
            are partition of the feasible polyhedron $\mathcal{X}_{0}$.
        \item  Value func. $J^{\star}(x(k))$ is convex and pw quad. on polyhedra.
    \end{itemize}
\end{whitebox}
\begin{whitebox}{\textbf{TRANSFORM P-NORM CFTOC TO LP}}
    \footnotesize
    \blue{$\ell_\infty$-Minimization} 
    \mathbox{
        \begin{aligned}
            \min_{x\in\mathbb{R}^m} &\lvert\lvert x \rvert\rvert_\infty \\
            \mathrm{subj.\ to }\ &Fx \leq g
        \end{aligned}
        \quad \Longleftrightarrow \quad
        \begin{aligned}
            \min_{x, t} & t \\
            \mathrm{subj.\ to }\ &-\boldsymbol{1_m}t \leq x \leq \boldsymbol{1_m}t, \ Fx \leq g
        \end{aligned}
    }
    \blue{Intuition} $-\boldsymbol{1_m}t \leq x \leq \boldsymbol{1_m}t$ bounds abs value of every elem. with scalar $t$

    \blue{$\ell_1$-Minimization} 
    \mathbox{
        \begin{aligned}
            \min_{x\in\mathbb{R}^m} &\lvert\lvert x \rvert\rvert_1 \\
            \mathrm{subj.\ to }\ &Fx \leq g
        \end{aligned}
        \quad \Longleftrightarrow \quad
        \begin{aligned}
            \min_{x\in\mathbb{R}^m, t\in\mathbb{R}^m} &\boldsymbol{1_m}^\top t \\
            \mathrm{subj.\ to }\ &-t \leq x \leq t, \ Fx \leq g
        \end{aligned}
    }
    \blue{Intuition} $\|x\|_1 = \sum_{i=1}^{n} \lvert x_i \rvert \leq \sum_{i=1}^{n} t_i = 1_n^\top t \ \leadsto \ -t \leq x \leq t$ \\
    bounds abs value of each component of $x$ with a component of vector $t$
\end{whitebox}
\begin{whitebox}{\textbf{CONSTRUCTION OF $\boldsymbol{\infty}$-NORM WITH SUBSTITUTION}}
    \blue{Cost}
    \mathbox{
        \min_z \ &\epsilon_N^x \textstyle\sum_{i-0}^{N-1} \epsilon_i^x + \epsilon_i^u \\
        \mathrm{subj.\ to }\ &-\boldsymbol{1}_n \epsilon_i^x \leq 
        \pm Q \left[ A^i x_0 + \textstyle\sum_{j=0}^{i-1}A^jBu_{i-1-j}\right] \\
        &-\boldsymbol{1}_r \epsilon_N^x \leq 
        \pm P \left[ A^N x_0 + \textstyle\sum_{j=0}^{N-1}A^jBu_{N-1-j}\right] \\
        &-\boldsymbol{1}_m \epsilon_i^u \leq Ru_i \\
        & x_i \in \mathcal{X}, \ u_i \in \mathcal{U}, \ x_f \in \mathcal{X}_f, \ x_0 = x(k)
    }
    \blue{Substitution} with $z:= \{\epsilon_0^x \dots \epsilon_N^x, \epsilon_0^u \dots \epsilon_{N-1}^u, u_0^\top \dots u_{N-1}^\top\}\in\mathbb{R}^s$,
    $s := (m+1)N + N + 1$
    \mathbox{
        \min_z \ &c^\top z \quad
        \mathrm{subj.\ to }\ \bar{G}z \leq \bar{w} + \bar{S}x(k)
    }
    \mathbox{
        \bar{G} = 
        \begin{bmatrix}
            G_\epsilon & G_u \\
            0 & G
        \end{bmatrix}, \quad
        \bar{S} = 
        \begin{bmatrix}
            S_\epsilon \\
            S
        \end{bmatrix}, \quad
        \bar{w} = 
        \begin{bmatrix}
            w_\epsilon \\
            w
        \end{bmatrix}
    }
    \blue{Solution} for given $x(k)$, $U^\star$ can be optained via LP solver
    
    \tcbsubtitle{\textbf{LP STATE FEEDBACK SOLUTION}}
    \begin{minipage}[c]{0.3\linewidth}
        \blue{MP-LP} \\
        multiparam. LP
    \end{minipage}
    \begin{minipage}[c]{0.65\linewidth}
        \mathboxplus{
            \min_z \ &c^\top z \ \
            \mathrm{subj.\ to }\ \bar{G}z \leq \bar{w} + \bar{S}x(k)
        }   
    \end{minipage}
    
    
    \blue{Properties}
    \begin{itemize}[leftmargin=1em]
        \footnotesize
        \item  First component of mp sol'n has form $u_0^\star = \kappa(x(0)), \quad \forall x(k)\in\mathcal{X}_0$
        $\kappa:\mathbb{R}^n\to\mathbb{R}^m$ cont. \& pw affine on Polyhedra
        \\ $\kappa(x) = F^j x + g^j \textrm{ if } x \in CR^j, \quad j=1,\dots,N^r$
        \item Polyhedral sets $CR^j = \{x \in \mathbb{R}^n \mid H^j x \leq K^j\}$ are partition of feasible Polyhedron $\mathcal{X}_0$ 
        \item In case of multiple optimizers, a pw affine control law exists
        \item $J^\star(x(0))$ is convex, pw linear on polyhedra 
    \end{itemize}
\end{whitebox}
\begin{whitebox}{\textbf{QUAD VS $\boldsymbol{1/\infty}$-NORM COST}}
    n = \# opt. var., FS = feas. set. \textbf{Solution is either}
    \begin{minipage}[t]{0.45\linewidth}
        \blue{Quadratic Cost}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item unique \& in interior of FS (no constraints active)
            \item unique \& on boundary of FS (at least 1 const. active)
        \end{itemize}
    \end{minipage}
    \begin{minipage}[t]{0.5\linewidth}
        \blue{Linear Cost}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item Unbounded
            \item unique at vertex of FS (at least n active constraints)
            \item multiple optima (at least 1 active const.)
        \end{itemize}
    \end{minipage}
\end{whitebox}