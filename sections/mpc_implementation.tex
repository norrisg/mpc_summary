\section{IMPLEMENTATION}

\begin{whitebox}{\textbf{EXPLICIT MPC}}
    \resizebox{\linewidth}{!}{
        \includegraphics[
        page = {3},
        trim = {0cm, 4.1cm, 0cm, 1.9cm},
        clip
        ]{11_numerical_short.pdf}
    }
    \blue{Recall: Quadratic Cost State Feedback Solution}
    \begin{whitebox}{}
        \footnotesize
        \textbf{MP-QP} -- Multiparametric Quadratic Program
        \begin{align*}
            J^{\star}(x(k)) =\min_{U} &\left[U^{\top} x(k)^{\top}\right]
            \left[\begin{smallmatrix} H & F^{\top} \\ F & Y \end{smallmatrix}\right]
            \left[U^{\top} x(k)^{\top}\right]^{\top} \\
            \mathrm{subj.\ to}\ &G U \leq w+E x(k)
        \end{align*}
        \textbf{Solution Properties} -- $J^{\star}(x(k))$ \textbf{convex} and \textbf{PW Quad.} on polyhedra.
    \end{whitebox}
    \blue{Active Set} for $l={1,\dots,m}$
    \begin{highlightbox}{}
        \footnotesize
        Define active set at $x$, $A(x)$, and it's complement $NA(x)$ as 
        \begin{align*}
            A(x) :=& \{j\in l: G_jz^\star(x) - S_jx = w_j \} \quad \text{(satisfied with eq.)} \\
            NA(x) :=& \{j\in l: G_jz^\star(x) - S_jx < w_j \} \quad \text{(strict inequality)}
        \end{align*}
    \end{highlightbox}
    \blue{Critical Region}
    \begin{highlightbox}{}
        \footnotesize
        $CR_A$ is set of parameters $x$ for which set $A\subseteq l$ of constraints i active at the optimum. 
        For given $\bar{x} \in \mathcal{K}^\star$ let $(A,NA) := (A(\bar{x}), NA(\bar{X}))$. Then 
        \begin{align*}
            CR_A := \{x\in\mathcal{K}^\star : A(x) = A\} \quad \text{(states share active set)}
        \end{align*}
    \end{highlightbox}
    \blue{Point Location}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item \textbf{Sequential Search} -- Computationally linear, very simple, works for all problems
            \item \textbf{Search Tree} -- Potentially logarithmic, significant offline processing (reasonable for <1k regions)
        \end{itemize}
    \end{whitebox}
    \blue{Remarks on Explicit MPC}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Linear MPC + Quad / Linear-norm cost $\leadsto$ Controller PWA func.
            \item Can pre-compute this function offline 
            \item Online evaluation of PWA function very fast (ns - $\mu$s)
            \item Can only do this for small systems (3-6 states, small horizon)
        \end{itemize}
    \end{whitebox}
    
    
\end{whitebox}

\begin{whitebox}{\textbf{ITERATIVE OPTIMIZATION METHODS}}

    \begin{minipage}[c]{0.60\linewidth}
        \blue{Generic Optimization Problem}
        {\footnotesize
            convex if $f:\mathbb{R}^n\to\mathbb{R}$ and set $\mathbb{Q}$ convex \\
            Analytical sol'n cannot be obtained except simplest cases
        }
    \end{minipage}
    \begin{minipage}[c]{0.35\linewidth}
        \mathbox{
            \mathrm{minimize}\ &f(x) \\
            \mathrm{subj.\ to}\ &x\in\mathbb{Q}
        }
    \end{minipage}
    
    
    \blue{Iterative Optimization Methods}
    \begin{highlightbox}{}
        \footnotesize
        Given initial guess $x^{(0)}$, produce sequence of iterates 
        \begin{align*}
            x^{(i+1)} = \psi(x^{(i)}, f, \mathbb{Q}), \quad i=0,\dots,m-1
        \end{align*}
        such that $\lvert f(x^{(m)}) - f(x^\star)\rvert \leq \epsilon \quad \text{and} \quad \mathrm{dist}(x^{(m)},\mathbb{Q}) \leq \delta$ \\
        where $\epsilon$ and $\delta$ are user defined tolerances
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{UNCONSTRAINED MINIMIZATION}}
    \blue{Optimality Conditions}
    \begin{whitebox}{}
        \footnotesize
        Assume $f(\cdot)$ diff'bar at $x^\star$. If $f$ convex, then $x^\star$ global min iff $\nabla f(x^\star)=0$
    \end{whitebox}

    \begin{minipage}[b]{0.45\linewidth}
        \blue{Descent Methods}
        \begin{highlightbox}{}
            \footnotesize
            \vspace{-1em}
            \begin{align*}
                x^{(i+1)} = x^{(i)} + h^{(i)} \Delta x^{(i)}
            \end{align*}
            with $f(x^{(i+1)})< f(x^{(i)})$
        \end{highlightbox}
        {\footnotesize
            \begin{itemize}[leftmargin=1em]
                \item $\Delta x$: step/search direction
                \item $h^{(i)}$: step size/length
                \item $f(x^{(i+1)})<f(x^{(i)})$ i.e $\Delta x^{(i)}$ is descent function
            \end{itemize}
        }
    \end{minipage}
    \begin{minipage}[b]{0.53\linewidth}
        \begin{whitebox}{}
            \begin{algorithmic}
                \scriptsize
                \State \textbf{Input} $x^{(0)}\in\mathrm{dom}(f)$
                \Repeat
                \State Compute descent dir. $\Delta x^{(i)}$
                \State Line Search: choose step size $h^{(i)}>0$ s.t $f(x^{(i)} + h^{(i)}\Delta x^{(i)}) < f(x^{(i)})$
                \State Update $x^{(i+1)} := x^{(i)} + h\Delta x^{(i)}$
                \Until{termination condition}
                \State (e.g $f(x^{(m)}) - f(x^\star)\leq \epsilon_1$)
            \end{algorithmic}
        \end{whitebox}
    \end{minipage}
    {\footnotesize
        \begin{itemize}[leftmargin=1em]
            \item $\exists h^{(i)}>0$ s.t $f(x^{(i+1)}) < f(x^{(i)})$ if $\nabla f(x^{(i)})^\top \Delta x^{(i)} < 0$
        \end{itemize}
    }

    \blue{Descent Direction}
    \begin{whitebox}{}
        \footnotesize
        \vspace{-0.25em}
        \begin{itemize}[leftmargin=1em]
            \item \textbf{Gradient descent} $x^{(i+1)} = x^{(i)} - h^{(i)}\nabla f(x^{(i)})$
            \begin{itemize}[leftmargin=1em]
                \item Assume $\nabla f$ Lipschitz-continuous $\lvert\lvert\nabla f(x) - \nabla f(y)\rvert\rvert \leq L \lvert\lvert x-y \rvert\rvert$
                \item Choose constant step size $h^{(i)}=1/L$
            \end{itemize}
            \item \textbf{Newton Step} $x^{(i+1)} = x^{(i)} + h^{(i)}\Delta x_{nt}$
            \begin{itemize}[leftmargin=1em]
                \item $\Delta x_{nt} = -(\nabla^2 f(x^{(i)}))^{-1} \nabla f(x^{(i)})$
                \item Exact Line Search $h^{(i)\star} = \mathop{\mathrm{argmin}}_{h>0} f(x^{(i)} + h^{(i)}\Delta x_{nt})$ \\
                Optimization in 1 var $\leadsto$ solve by bisection, time consuming
                \item Inexact Line search: find $h^{(i)}$ that decreases $f$ by some amount
            \end{itemize}
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{CONSTRAINED MINIMIZATION}}
    \blue{Projected Gradient Methods}
    \begin{whitebox}{}
        \footnotesize
        \begin{minipage}[c]{0.68\linewidth}
            \textbf{Incorporate Constraints in Gradient Step}
            \mathbox{
                x^{(i+1)} = {\color{color2}\pi_{\mathbb{Q}}(} x^{(i)} - h^{(i)} \nabla f(x^{(i)}){\color{color2} )}
            }
            \textbf{Projection} $\pi_{\mathbb{Q}} = \mathop{\mathrm{argmin}}_x \frac{1}{2}\| x-y\|_2^2 \ \mathrm{s.t}\ x\in\mathbb{Q}$ 
            \begin{itemize}[leftmargin=1em]
                \item Simple input constraints: ezpz
                \item State constraints: hard $\leadsto$ solve for dual
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.30\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {41},
                trim = {9.0cm, 0.75cm, 0.5cm, 6cm},
                clip
                ]{11_numerical_short.pdf}
            }
        \end{minipage}
    \end{whitebox}
    \blue{Interior-Point Methods}
    \begin{whitebox}{}
        \footnotesize
        \textbf{System} $\min \ f(x) \ \mathrm{s.t.}\ g_i(x) \leq 0, i=1,\dots,m$

        \textbf{Assumptions} $f,g_i$ convex, twice cont. diff'bar. $f(x^\star)$ is finite and attained, stict feasiblity $\exists g(\tilde{x})<0$, feasible set closed \& compact

        \textbf{Idea} Reformulate as unconstrained problem
    \end{whitebox}
    \blue{Primal-Dual Interior-Point Methods}
    \begin{whitebox}{}
        \footnotesize 
        \begin{minipage}[c]{0.55\linewidth}
            \textbf{Idea -- Iteratively solve \underline{relaxed} KKT system} leave $\lambda_i^\star$ as variables, linearize and solve resulting sytem of linear eqns at each iteration

            \textbf{Search Direction} $\Delta[x,\nu,\lambda,s](v)$
            \begin{itemize}[leftmargin=1em]
                \item $v=0$ pure Newton direction ``predictor''/``affine-scaling''
                \item $v=\kappa \boldsymbol{1}$ centering direction, approach central path
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.43\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {47},
                trim = {2.5cm, 3.0cm, 2.5cm, 2.0cm},
                clip
                ]{11_numerical_short.pdf}
            }
            $\Rightarrow$ combine via \textbf{centering parameter} $\sigma \in (0,1)$
        \end{minipage}
    \end{whitebox}
\end{whitebox}