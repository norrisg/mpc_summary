\section{PRACTICAL MPC}

\begin{whitebox}{\textbf{REFERENCE TRACKING}}
    \blue{System} $x(k+1) = Ax(k) + Bu(k), \quad x\in\mathbb{R}^{n_x}, u\in\mathbb{R}^{n_u}$
    \blue{Constraints} $\mathcal{X} = \{x \mid G_x x \leq h_x\}, \mathcal{U} = \{u \mid G_u u \leq h_u\}$
    \tcbsubtitle{\textbf{STEADY-STATE TARGET PROBLEM}}
    \begin{highlightbox}{}
        \textbf{Target Condition}
        \begin{align*}
            \begin{aligned}
                x_s &= Ax_s + Bu_s \\
                z_s &= Hx_s = r
            \end{aligned}
            \ \Longleftrightarrow \
            \begin{aligned}
                \begin{bmatrix}
                    \mathbb{I} - A & -B \\
                    H & 0
                \end{bmatrix}
                \begin{bmatrix}
                    x_s \\
                    u_s
                \end{bmatrix}
                =
                \begin{bmatrix}
                    0 \\
                    r
                \end{bmatrix}
            \end{aligned}
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item In presence of constraints, $(x_s,u_s)$ must satisfy them
            \item In case of multiple feas. $u_s$, compute `cheapest' %steady-state $(x_s, u_s)$
            \begin{align*}
                \min u_s^\top R_s u_s \quad \mathrm{subj.\ to }\ \textrm{[Target Condition]}, x_s \in \mathcal{X}, u_s \in \mathcal{U}
            \end{align*}
            \item In general, asssume target problem is feasible 
            \item If no sol'n $\exists$: compute reachable point `closest' to $r$
            \begin{align*}
                \min (Hx_s - r)^\top Q_s (H x_s - r) \quad \mathrm{subj.\ to }\ x_s = Ax_s + Bu_s
            \end{align*}
        \end{itemize}
    \end{highlightbox}

    \tcbsubtitle{\textbf{MPC FOR REFERENCE TRACKING}}
    \blue{MPC Design}
    \mathbox{
        \textstyle\min_U &\lvert\lvert z_N - Hx_s\rvert\rvert_{P_z}^2 
        + \textstyle\sum_{i=1}^{N-1} \lvert\lvert z_i - Hx_s \rvert\rvert_{Q_z}^2 
        + \lvert\lvert u_i - u_s \rvert\rvert_{R}^2 \\
        \mathrm{subj.\ to }\ &\mathrm{[model, constraints]}, x_0 = x(k)
    }
    \blue{Delta Form.} 
    {\footnotesize
    Set pt. tracking $\xrightarrow{\mathrm{Coord. Trans.}}$ Regulation Problem
    }
    \begin{highlightbox}{}
        \begin{align*}
            \left.
            \begin{aligned}
                \Delta x :=& x - x_s \\
                \Delta u :=& u - u_s
            \end{aligned}
            \ \right\rvert \ 
            \begin{aligned}
                G_x \Delta x &\leq h_x - G_x x_s \\
                G_u \Delta u &\leq h_u - G_u u_s
            \end{aligned}
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item Obtain target steady-state corresponding to reference $r$
            \item Initial state $\Delta x(k) = x(k) - x_s$
            \item Apply reg problem to new system in $\Delta$-Formulation
        \end{itemize}
        \begin{align*}
            \min &\left[V_f(\Delta x_N) + \textstyle\sum_{i=1}^{N=1} 
            \Delta x_i^\top Q \Delta x_i + \Delta u_i^\top R \Delta u_i\right] \\
            \mathrm{subj.\ to }\ &\Delta x_{i+1} = A \Delta x_i + B \Delta u_i, \quad
            G_x \Delta x_i \leq h_x - G_x x_s \\
            &G_u \Delta u_i \leq h_u - G_u u_s, \quad
            \Delta x_N \in \mathcal{X}_f, \quad \Delta x_0 = \Delta x(k)
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item Find optimal sequence of $\Delta U^\star$
            \item Input applied to system $u_0^\star = \Delta u_0^\star + u_s$
        \end{itemize}
    \end{highlightbox}
    \blue{Convergence}
    \begin{highlightbox}{}
        Assume target feasible with $x_s \in \mathcal{X}, u_s \in \mathcal{U}$, 
        choose terminal weight $V_f(x)$ and constraint $\mathcal{X}_f$ as in regulation case satisfying
        \begin{itemize}[leftmargin=1em]
            \item $\mathcal{X}_f \subseteq \mathcal{X}, K x \in \mathcal{U} \quad \forall x \in \mathcal{X}_f$
            \item $V_f(x(k+1)) - V_f(x(k)) \leq -l (x(k), Kx(k)) \quad \forall x \in \mathcal{X}_f$
        \end{itemize}
        If in addition the target reference $x_s, u_s$ is such that
        \begin{itemize}[leftmargin=1em]
            \item $x_s \oplus \mathcal{X}_f \subseteq \mathcal{X}, K\Delta x + u_s \in \mathcal{U}, \quad \forall \Delta x \in \mathcal{X}_f$
        \end{itemize}
        then CL system converges to target reference
        \begin{align*}
            x(k) \to x_s, z(k) = Hx(k) \xrightarrow{k\to\infty}r
        \end{align*}
    \end{highlightbox}
    {\footnotesize 
        \blue{Proof} 
        \begin{itemize}[leftmargin = 1em]
            \item Invariance under local ctrol law inherited from regulation case 
            \item Constraint satisfaction provided by extra conditions
            \begin{itemize}
                \item $x_s \oplus \mathcal{X}_f \subseteq \mathcal{X} \to x\in \mathcal{X} \forall \Delta \in\mathcal{X_f}$
                \item $K\Delta x + u_s \in \mathcal{U} \forall \Delta x \in \mathcal{X}_f \to u\in\mathcal{U}$
            \end{itemize}
            \item Fron asympt stability of the regulation problem: $\Delta x (k) \xrightarrow{k\to\infty}0$
        \end{itemize}
    }
    \blue{Terminal Set}
    \begin{itemize}[leftmargin=1em]
        \item Set of feasible targets may be significantly reduced. \\
        Enlarge set of feasible targets by scaling terminal set for regulation 
        $\mathcal{X}_f^{\mathrm{scaled}} = \alpha \mathcal{X}_f$
        \item Invariance maintained if $\mathcal{X}_f$ invariant $\leadsto$ so is $\alpha \mathcal{X}_f$
        \item Choose $\alpha$ s.t. $x,u$ constraints still satisfied $\leadsto$ scaling target dependent
        \item Targets at the boundary of the constraints: $x_N = x_s$, correspons to 0-terminal set in regulation case
    \end{itemize}
\end{whitebox}
\begin{whitebox}{\textbf{MPC FOR REFERENCE TRACKING WITHOUT OFFSET}}
    \blue{Augmented Model}
    \begin{highlightbox}{}
        \begin{align*}
            x_{k+1} = Ax_k + Bu_k + B_d d_k, \quad
            d_{k+1} = d_k, \quad
            y_k = Cx_k + C_d d_k
        \end{align*}
        \textbf{Observability} of aug. system: 
        $\mathrm{rank}\left(\left[\begin{smallmatrix}
            A - \mathbb{I} & B_d \\ C & C_d
        \end{smallmatrix}\right]\right)=n_x + n_d$ 

        \textbf{Inuition} At steady-state 
        $\left[\begin{smallmatrix}
            A- \mathbb{I} & B_d \\ C & C_d
        \end{smallmatrix}\right]
        \left[\begin{smallmatrix}
            x_s \\ d_s
        \end{smallmatrix}\right]
        = 
        \left[\begin{smallmatrix}
            0 \\ y_s
        \end{smallmatrix}\right]$, $y_s, d_s$ unique
    \end{highlightbox}
    \blue{Linear State Estimation}
    \begin{highlightbox}{}
        
        \begin{minipage}[c]{0.25\linewidth}
            \textbf{Observer For Augmented Model}
        \end{minipage}
        \begin{minipage}[c]{0.70\linewidth}
            \begin{align*}
                \footnotesize
                &\begin{bmatrix}
                    \hat{x}(k\!+\!1) \\
                    \hat{d}(k\!+\!1)
                \end{bmatrix}
                \!=\! 
                \begin{bmatrix}
                    A & B_d \\
                    0 & \mathbb{I}
                \end{bmatrix}
                \begin{bmatrix}
                    \hat{x}(k) \\
                    \hat{d}(k)
                \end{bmatrix}
                \!+\!
                \begin{bmatrix}
                    B \\ 
                    0
                \end{bmatrix}
                \!u(k) \\ 
                &+
                \begin{bmatrix}
                    L_x \\
                    L_d
                \end{bmatrix}
                (-y(k) \!+\! C \hat{x}(k) \!+\! C_d \hat{d}(k))
            \end{align*}
        \end{minipage}

        \textbf{Error Dynamics} $\Rightarrow$ choose $L$ s.t error dynamics asympt. stable
        \begin{align*}
            \footnotesize
            \begin{bmatrix}
                x(k\!+\!1) - \hat{x}(k\!+\!1) \\
                d(k\!+\!1) - \hat{d}(k\!+\!1)
            \end{bmatrix}
            = \left( 
            \begin{bmatrix}
                A & B_d \\
                0 & \mathbb{I}
            \end{bmatrix}
            + 
            \begin{bmatrix}
                L_x \\
                L_d
            \end{bmatrix}
            \begin{bmatrix}
                C & C_d
            \end{bmatrix}
            \right) 
            \begin{bmatrix}
                x(k) - \hat{x}(k) \\
                d(k) - \hat{d}(k)
            \end{bmatrix}
        \end{align*}
    \end{highlightbox}
\end{whitebox}
\begin{whitebox}{\textbf{MPC FOR REFERENCE TRACKING WITHOUT OFFSET}}
    \blue{Observer Steady-State}
    \begin{whitebox}{}
        Suppose observer asympt. stable and $n_y = n_d$
        \begin{align*}
            \begin{bmatrix}
                A-\mathbb{I} & B \\
                C & 0
            \end{bmatrix}
            \begin{bmatrix}
                \hat{x}_\infty \\
                u_\infty
            \end{bmatrix}
            = 
            \begin{bmatrix}
                -B_d \hat{d}_\infty \\
                y_\infty - C_d \hat{d}_\infty
            \end{bmatrix}
        \end{align*}
        $\leadsto$ Observer output $C\hat{x}_\infty + C_d \hat{d}_\infty$ tracks $y_\infty$ without offset
    \end{whitebox}
    \blue{Offset-Free Tracking}
    \begin{whitebox}{}
        \textbf{Goal} Track constant $r$: $z(k) = Hy(k) \to r$ as $k\to\infty$
        
        \textbf{Steady-State Condition}
        \begin{align*}
            x_s = Ax_s + Bu_s + B_d \hat{d}_\infty, \quad z_s = H(Cx_s + C_d \hat{d}_\infty) = r
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item Best forecast for $d_\infty$ is current estimate $\hat{d}_\infty = \hat{d}$
            \item Same Procedure for regulation case with $r=0$
        \end{itemize}
        \textbf{Offset-Free Tracking Condition}
        \begin{align*}
            \begin{bmatrix}
                A-\mathbb{I} & B \\
                HC & 0
            \end{bmatrix}
            \begin{bmatrix}
                x_s \\
                u_s
            \end{bmatrix}
            =
            \begin{bmatrix}
                -B_d \hat{d} \\
                r - HC_d \hat{d}
            \end{bmatrix}
        \end{align*}
    \end{whitebox}
    \blue{Offset-Free Tracking Procedure}
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.5em]
            \item Estimate $\hat{x}$ \& $\hat{d}$
            \item Obtain $(x_s, u_s)$ from steady-state tgt problem using $\hat{d}$
            \item Solve MPC problem for tracking using $\hat{d}$ 
            $\tilde{x}_i := x_i - x_s$, $\tilde{u}_i = u_i - u_s$
        \end{enumerate}
        \begin{align*}
            \min_U &V_f(\tilde{x}_N) + \textstyle\sum_{i=0}^{N-1} (\tilde{x}_i)^\top Q (\tilde{x}_i) 
            + (\tilde{u}_i)^\top R (\tilde{u}_i) \\
            \mathrm{subj.\ to }\ &x_{i+1} = Ax_i + Bu_i + B_d d_i, \quad
            d_{i+1} = d_i \\
            &x_i \in \mathcal{X}, \quad u_i \in \mathcal{U}, \\
            &x_0 = \hat{x}(k), \quad d_0 = \hat{d}(k), \quad x_n - x_s \in \mathcal{X}_f 
        \end{align*}
    \end{whitebox}

    \blue{Offset-Free Tracking: Main Result}
    \begin{highlightbox}{}
        \footnotesize
        With $u_0^\star = \kappa(\hat{x}(k),\hat{d}(k), r) = \kappa(\cdot)$.
        Assuming $n_d = n_y$,  RHC recursively feas., unconstrained for $k \geq j$, $j\in\mathbb{N}^+$, CL system:
        \begin{align*}
            x(k+1) &= Ax(k) + B\kappa(\cdot) + B_d d\\
            \hat{x}(k+1) &= (A + L_x C)\hat{x}(k) + (B_d + L_x C_d)\hat{d}(k) + B\kappa(\cdot) - L_x y(k) \\
            \hat{d}(k+1) &= L_d C \hat{x}(k) + (\mathbb{I} + L_d C_d)\hat{d}(k) - L_d y(k)
        \end{align*}
        converges ($(\hat{x}, \hat{d}) \xrightarrow{k\to\infty} (x_\infty, d_\infty)$), then $z(k) = Hy(k) \xrightarrow{k\to\infty}r$
    \end{highlightbox}
\end{whitebox}
\begin{whitebox}{\textbf{ENLARGING FEASIBLE SET -- NO TERMINAL SET}}
    \blue{Motivation} Term. constraints reduces feasible set \\
    \blue{Goal} MPC without term. constraint with guaranteed stability \\
    \blue{Note} Feasible set without term. constraint not invariant

    \blue{MPC Without Terminal Set}
    \begin{highlightbox}{}
        Can remove Term. constraint while maintaining stability \textbf{if}
        \begin{itemize}[leftmargin=1em]
            \item Initial state lies in sufficiently small subset of feasible set 
            \item $N$ sufficiently large
        \end{itemize}
        s.t term. state satisfies term. const. without envorcining it in the optimization. 
        $\leadsto$ Sol'n of finite-horizon MPC problem corresponds to $\infty$-horizon sol'n
    \end{highlightbox}
    {\footnotesize
        \blue{Advantage} -- Controller defined in larger feasible set \\
        \blue{Disadvantage} -- Characterization of region of attaction of specification of required horizon length extremely difficult
        \begin{itemize}[leftmargin=1em]
            \item Term constr provides sufficient cond. for stab: RoA w/o term constr may be larger than w/
            \item In practice: Enlarge horizon and check stability by sampling
            \item $N\uparrow$ $\leadsto$ RoA approachees max control invar. set
        \end{itemize}
    }
\end{whitebox}
\begin{whitebox}{\textbf{ENLARGING FEASIBLE SET -- SOFT CONSTRAINTS}}
    \blue{Motivation} Input constraints usually `hard', state constraints rarely `hard' $\leadsto$ breakable \\
    \blue{Goal} Min size \& duration of violation (\textbf{usually conflict!})
    \blue{MPC Problem Setup}
    \mathboxplus{
        \min_u &\left[ x_N^\top P x_N + \boldsymbol{l_\epsilon(\epsilon_N)} + \textstyle\sum_{i=0}^{N-1} x_i^\top Q x_i + u_i^\top R u_i + \boldsymbol{l_\epsilon(\epsilon_i)} \right] \\
        \textrm{s.t. } &x_i = Ax_i + Bu_i, \quad
        H_x x_i \leq k_x \boldsymbol{+ \epsilon_i}, \
        H_u u_i \leq k_u, \quad
        \boldsymbol{\epsilon_i \geq 0}
    }
    \blue{Requirement on $\boldsymbol{l_\epsilon}$} 
    \begin{highlightbox}{}
        \footnotesize
        \vspace{-0.5em}
        \begin{align*}
            \begin{array}{c | c}
                \text{Original Problem} & \text{``Softened'' Problem} \\
                \begin{aligned}
                    \min_z \ f(z) \quad
                    \mathrm{s.t}\ g(z) \leq 0
                \end{aligned}
                &
                \begin{aligned}
                    \min_z \ f(z) + l_\epsilon(\epsilon) \quad
                    \mathrm{s.t}\ g(z) \leq \epsilon, \ \ \epsilon \geq 0
                \end{aligned}
            \end{array}
        \end{align*}
        If original problem has feasible solution $z^\star$, Softened problem should have same solution $z^\star$, and $\epsilon=0$. \\
        \textbf{Note} $l_\epsilon(\epsilon_i) = s\epsilon_i^2$ does \textbf{not} fulfill requirement
    \end{highlightbox}

    \blue{Choice of Penalty}
    \begin{itemize}[leftmargin=1em]
        \item \textbf{Quad. Penalty} $l_\epsilon(\epsilon_i) = \epsilon_i^\top S \epsilon_i$ (e.g $S = Q$)
        \item \textbf{Quad. + Linear Penalty} $l_\epsilon(\epsilon_i) = \epsilon_i^\top S \epsilon_i + v\lvert\lvert \epsilon_i \rvert\rvert_{1/\infty}$
    \end{itemize}
    
    \blue{Exact Penalty Function}
    \begin{highlightbox}{}
        $l_\epsilon(\epsilon) = v \cdot \epsilon$ satisfies requirement for any $v > \lambda^\star \geq 0$, 
        where $\lambda^\star$ is optimal Lagrange multiplier for original problem
    \end{highlightbox}
    {\footnotesize
        \begin{itemize}[leftmargin=1em]
            \item In practice, combined cost used for exact penalty and tuning capabilities
        \end{itemize}
    }
    \blue{Tuning}
    \begin{whitebox}{}
        \begin{enumerate}[leftmargin=1.5em]
            \item Minimize violation over horizon:
        \end{enumerate}
        \begin{align*}
            \epsilon^{\mathrm{min}} = &\textstyle\mathop{\mathrm{argmin}}_{u,\epsilon}
            \sum_{i=0}^{N-1} \epsilon_i^\top S \epsilon_i + v^\top \epsilon_i, \quad 
            \textrm{s.t } x_{i+1} = Ax_i + Bu_i \\
            &H_x x_i \leq k_x + \epsilon_i, \quad H_u u_i \leq k_u, \quad \epsilon_i \geq 0
        \end{align*}
        \begin{enumerate}[leftmargin=1.5em, resume]
            \item Optimize Controller performance
        \end{enumerate}
        \begin{align*}
            \textstyle\min_u \ &x_N^\top P x_N + \textstyle\sum_{i=0}^{N-1} x_i^\top Q x_i + u_i^\top R u_i \\
            \textrm{s.t } & x_{i+1} = Ax_i + Bu_i, \quad H_x x_i \leq k_x + \epsilon_i^{\mathrm{min}}, \quad
            H_u u_i \leq k_u
        \end{align*}
    \end{whitebox}
    \blue{Note} Standard SC MPC does not provide stability guarantee for OL unstable sys.
\end{whitebox}