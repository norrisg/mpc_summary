\section{ROBUST MPC I}
        
\begin{whitebox}{\textbf{UNCERTAINTY MODELS}}
    \blue{Motivation}
    {\footnotesize
        Random Noise $w$ changes sys. evolution,
        Model structure unknown,
        Unknown parameters $\theta$ impact dynamics
    }

    \blue{Uncertain Constrained System}
    \mathbox{
        x(k\!+\!1) \!=\! g(x(k), u(k), w(k); \theta), \quad 
        x,u,w,\theta \in \mathcal{X},\mathcal{U},\mathcal{W},\Theta
    }
    \blue{Additive Bounded Noise System}
    \mathbox{
        x(k\!+\!1) \!=\! Ax(k) + Bu(k) + w(k), \quad 
        x,u,w \in \mathcal{X},\mathcal{U},\mathcal{W}
    }
\end{whitebox}
\begin{whitebox}{\textbf{IMPACT OF BOUNDED ADDITIVE NOISE}}
    \blue{Goals}
    \begin{highlightbox}{}
        \textbf{Design $u(k)=\kappa(x(k))$ s.t the system}
        {\footnotesize
            \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\alph*)}]
                \item \textbf{Satisfies constraints}: $\{x(k)\}\subset \mathcal{X}, \{u(k)\}\subset \mathcal{U}$ for all disturbances
                \item \textbf{Is Stable}: converges to neighbourhood of origin
                \item \textbf{Optimizes (expected/worst-case) `Performance'}
                \item \textbf{Maximizes Set} $\{x(0)\mid \textrm{Condition 1-3 met}\}$
            \end{enumerate}
        }
    \end{highlightbox}
    
    \blue{Uncertain State Evolution}
    \mathbox{
        \phi_i = \underbrace{A^i x_0 + \textstyle\sum_{j=0}^{i-1} A^j B u_{i-1-j}}_{x_i \ \equiv \ \textrm{Nominal System}}
        + \underbrace{\textstyle\sum_{j=0}^{i-1} A^j w_{i-1-j}}_{\textrm{Disturbance Offset}}
    }
    \tcbsubtitle{\textbf{(c) -- OPTIMIZES PERFORMANCE}}
    \blue{Cost to Minimize}
    \begin{highlightbox}{}
        \textbf{Cost now func of Disturbance} $\leadsto$ Need to eliminate $W$
        \begin{align*}
            J(x_0, U, W) := l_f(\phi_N(x_0,U,W)) + \textstyle\sum_{i=0}^{N-1}l(\phi_i(x_0,U,W),u_i) 
        \end{align*}
        \textbf{Several Options}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item Minimize expected value $J_N(x_0,U) = \mathbb{E}\{J(x_0,U,W)\}$
            \item Take worst case $J_N(x_0,U) := \max_{W\in\mathcal{W}^{N=1}}J(x_0,U,W)$
            \item \textbf{Take Nominal Case} $J_N(x_0,U) := J(x_0,U,0)$
        \end{itemize}
    \end{highlightbox}

    \tcbsubtitle{\textbf{(a) -- SATISFIES CONSTRAINTS}}
    \blue{Robust Constraint Satisfaction}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item \textbf{State \& Input Constraints} for $i=0,\dots,N-1$, \\
            Enforce constraints explicitly by imposing $\phi_i \in \mathcal{X},\  u_i \in \mathcal{U}, \ \forall W \ \in\mathcal{W}^N$
            \item \textbf{Terminal Constraints} for $i=N,\dots$ \\
            Enforce constraints implicitly $\phi_N \in \text{ robust invariant set }\mathcal{X}_f$, $K\mathcal{X}_f \in \mathcal{U}$ for $\phi_{i+1} = (A+BK)\phi_i + w_i$
        \end{itemize}
    \end{whitebox}
    \blue{Robust Positive Invariant Set}
    \begin{highlightbox}{}
        Set $\mathcal{O}^\mathcal{W}$ said to be robust pos. invar. for autonomous system $x(k+1) = g(x(k), w(k))$ if
        \begin{align*}
            x\in\mathcal{O}^\mathcal{W} \ \Rightarrow \ g(x,w)\in\mathcal{O}^\mathcal{W}, \ \forall w\in\mathcal{W}
        \end{align*}
    \end{highlightbox}
    \blue{Robust Pre-Set}
    \begin{highlightbox}{}
        Given set $\Omega$ and dynamic system $x(k+1) = g(x(k), w(k))$, 
        \begin{align*}
            \mathrm{pre}^{\mathcal{W}}(\Omega) :=& \{x \mid g(x,w)\} \in \Omega \ \forall w \in \mathcal{W}
        \end{align*}
    \end{highlightbox}

    \blue{Computing Robust Pre-Sets for Linear Systems} \\
    System $Ax(k) + w(k)$, set $\Omega:= \{x \mid Fx \leq f\}$
    \begin{whitebox}{}
        \footnotesize
        \vspace{-1em}
        \begin{align*}
            \mathrm{pre}^{\mathcal{W}}(\Omega) 
            = 
            \{ x \mid FAx \leq f - {\color{color2} \max_{w\in\mathcal{W}}Fw} \}
            = 
            \{ x \mid FAx \leq f - {\color{color2} h_{\mathcal{W}^i}(F)} \}
        \end{align*}
    \end{whitebox}

    \blue{Robust Invariant Set Conditions}
    \begin{highlightbox}{}
        Set $\mathcal{O}^\mathcal{W}$ is robust positive invariant set \textbf{iff}
        \begin{align*}
            \mathcal{O}^\mathcal{W} \subseteq \mathrm{pre}^\mathcal{W}(\mathcal{O}^\mathcal{W})
            \ \Leftrightarrow \
            \mathrm{pre}^\mathcal{W}(\mathcal{O}^\mathcal{W}) \cap \mathcal{O}^\mathcal{W} = \mathcal{O}^\mathcal{W}
        \end{align*}
    \end{highlightbox}
    \blue{Robust Constraint Satisfaction}
    \begin{whitebox}{}
        Ensure constraints are satisfied for MPC sequence 
        \begin{align}
            \phi_i(x_0, U, W) = \left\{ x_i + \textstyle\sum_{j=0}^{i-1} A^j w_{i-1-j} \mid W\in\mathcal{W}^i \right\} \subseteq \mathcal{X}
            \label{eqn:tightenedconstr}
        \end{align}
        Assume $\mathcal{X} = \left\{ x \mid Fx \leq f \right\}$ (polyhedron)
        \begin{align*}
            Fx_i \leq f - h_{\mathcal{W}^i}\left( F\textstyle\sum_{j=0}^{i-1}A^j \right)
        \end{align*}
    \end{whitebox}
    {\footnotesize
        \blue{ACHTUNG} \textbf{Must ensure term state contained in robust invariant set}

        \blue{Intuition} Tightening constraints on nominal system
    }
    
\end{whitebox}
\begin{whitebox}{\textbf{SET OPERATORS}}

    \begin{minipage}[c]{0.48\linewidth}
        \blue{Minkowski Sum}
        \begin{highlightbox}{}
            \footnotesize
            \vspace{-1em}
            \begin{align*}
                A \oplus B := \{ x + y \mid x \in A, y \in B\}
            \end{align*}
        \end{highlightbox}
    \end{minipage}
    \begin{minipage}[c]{0.50\linewidth}
        \blue{Pontryagin Difference}
        \begin{highlightbox}{}
            \footnotesize
            \vspace{-1em}
            \begin{align*}
                A \ominus B := \{ x \mid x + e \in A \ \forall e \in B\}
            \end{align*}
        \end{highlightbox}
    \end{minipage}

    \vspace{0.25em}

    {\footnotesize
        \blue{ACHTUNG} $A \ominus B \oplus B \subseteq A$
    }

    \blue{Robust Constraint Satisfaction} \\
    Eqn. (\ref{eqn:tightenedconstr}) can be rewritten 
    $\phi_i \in x_i \oplus (\mathcal{W} \oplus \dots A^{i-1}\mathcal{W}) \subseteq \mathcal{X}$
    \begin{highlightbox}{}
        \textbf{Enforcing this cond. requires Tightened Constraints}
        \begin{align*}
            x_i \in \mathcal{X} \ominus \left( \textstyle\bigoplus_{j=0}^{i-1} A^j \mathcal{W} \right)
        \end{align*}
    \end{highlightbox}
\end{whitebox}

\begin{whitebox}{\textbf{ROBUST OPEN-LOOP MPC}}
    \blue{Robust Open-Loop MPC}
    \begin{highlightbox}{}
        \footnotesize
        \begin{align*}
            \min_U &\left[ l_f(x_N) + \textstyle\sum_{i=0}^{N-1} l(x_i, u_i) \right] \\
            \mathrm{subj.\ to }\ &x_{i+1} = A x_i + B u_i \\
            &x_i \in \mathcal{X} \ominus ( \textstyle\bigoplus_{j=0}^{i-1} A^j \mathcal{W} ), \quad
            u_i \in \mathcal{U} \\
            &x_0 = x(k), \quad x_N \in \mathcal{X}_f \ominus ( \textstyle\bigoplus_{j=0}^{N-1} A^j \mathcal{W} )
        \end{align*}
        {\footnotesize
            $\mathcal{X}_f \subseteq \mathcal{X}$ robust pos invar set 
            for system $(A+BK)x(k) + w(k)$ with $w\in\mathcal{W} \ \forall k$
            for some stabilizing $K$, and $Kx\in\mathcal{U} \ \forall x\in\mathcal{X}_f$ 
        }
    \end{highlightbox}
    {\footnotesize 
        \blue{Intuition} Nominal MPC, but with tigher state constraints 

        \blue{Open-Loop?} Not accounting for FB during solving, just plan ahead for $w$

        \blue{Achtung}
        \begin{itemize}[leftmargin=1em]
            \item Unstable systems $A^{i-1}\mathcal{W}$ grows $\leadsto$ use `pre-stabilization' $u_i = Kx_i + u_i$
            \item Potentially \textbf{very small region of attraction}, particularly for unstable sys 
        \end{itemize}
    }

    \blue{Robust Invariance}
    \begin{highlightbox}{}
        \footnotesize
        If $U^\star(x(k))$ is optimizer of robust OL MPC problem for $x(k)\in\mathcal{X}$, 
        then system $Ax(k) + Bu_0^\star(x(k)) + w(k) \in \mathcal{X}$ for all $w\in\mathcal{W}$
    \end{highlightbox}
\end{whitebox}