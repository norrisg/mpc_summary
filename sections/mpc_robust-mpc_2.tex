\section{ROBUST MPC II}
        
\begin{whitebox}{\textbf{CLOSED-LOOP PREDICTIONS}}
    \blue{Goal} optimize over seq. of funcs
    $\{u_0, \mu_1(\cdot),\dots,\mu_{N-1}(\cdot)\}$ 
    with ($\mu_i(x_i): \mathbb{R}^n \to \mathbb{R}^m$ control policy)

    \blue{Problem} \textbf{Can't optimize over arbitrary functions!}

    \blue{Solution} assume some structure on functions $\mu_i$
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Pre-Stabilization] $\mu_i(x_i) = Kx_i + v_i$ \\
            Fixed K, s.t $A+BK$ stable $\leadsto$ Simple, often conservative
            
            \item[Linear Feedback] $\mu_i(x_i) = K_i x_i + v_i$ \\
            Optimize over $K_i, v_i$, $\leadsto$ \textbf{Non-Convex} -- Extremely difficult to solve

            \item[Disturbance Feedback] $\mu_i(x_i) = \sum_{j=0}^{i-1}M_{ij}w_j + v_i$ \\
            Optimize over $M_{ij}, v_i$ $\leadsto$ Equiv to linear feedback but \textbf{Convex}
            $\leadsto$ Effective, but computationally intense

            \item[Tube-MPC] $\mu_i(x_i) = v_i + K(x_i - \bar{x}_i)$ \\
            Fixed K, s.t $A+BK$ stable $\leadsto$ Optimize over $\bar{x}_i, v_i$
            $\leadsto$ Simple, can be effective
        \end{description}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{TUBE-MPC}}
    \blue{System} 
    \mathbox{
        x(k+1) = Ax(k) + Bu(k) + w(k) \quad x,u \in \mathcal{X},\mathcal{U} \quad w\in\mathcal{W}
    }

    \blue{Idea} Seperate available control authority into 2 parts 
    \begin{highlightbox}{}
        \footnotesize 
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Portion that steers nominal sys to origin $z(k+1) = Az(k) + Bv(k)$
            \item Portion that compensates for deviations from this system $u_i = K(x_i - z_i) + v_i$
            (keeps real traj close to nominal), for some linear $K$, which stabilizes nominal system
        \end{enumerate}
    \end{highlightbox}
    {\footnotesize
        $\leadsto$ \textbf{Fix linear FB K offline} and optimize over nominal trajectory $\{v_0,\dots,v_{N-1}\}$ 
        $\leadsto$ convex problem
    }
    
    \blue{Error Dynamics} 
    \begin{whitebox}{}
        \footnotesize
        Define $e_i := x_i - z_i \leadsto e_{i+1} = (A+BK)e_i + w_i$ \\
        Bound maximum error, how far `real' traj from nominal 
        \begin{align*}
            e_{i+1} = (A+BK)e_i + w_i \qquad w_i \in \mathcal{W}
        \end{align*}
        Dynamics $A+BK$ are stable, set $\mathcal{W}$ bounded 
        $\leadsto$ Set $\mathcal{E}$ s.t $e$ stays inside $\forall k$ $\leadsto$ want `minimal robust invariant set'
    \end{whitebox}
    \blue{Tube-MPC Procedure}
    \begin{highlightbox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\alph*)}]
            \item Compute set $\mathcal{E}$ that error remains inside 
            \item Modify constraints on nominal traj $\{z_i\}$
            \item Formulate as convex optimization problem
        \end{enumerate}
    \end{highlightbox}

    \tcbsubtitle{\textbf{(a) -- MINIMUM ROBUST INVARIANT SET}}

    
    \begin{minipage}[b]{0.45\linewidth}
        \blue{mRPI -- Minimum \break Robust Invariant Set}
        \begin{highlightbox}{}
            \vspace{-0.5em}
            \begin{align*}
                F_\infty =& \textstyle\bigoplus_{j=0}^\infty A^j \mathcal{W} \\
                F_0 :=& \{0\}
            \end{align*}
            If $F_n = F_{n+1} \Rightarrow F_n = F_\infty$
        \end{highlightbox}
    \end{minipage}
    \begin{minipage}[b]{0.50\linewidth}
        \begin{whitebox}{}
            \begin{algorithmic}
                \footnotesize
                \State $\Omega_0 \leftarrow \{0\}$
                \Loop
                \State $\Omega_{i+1} \leftarrow \Omega_i \oplus A^i \mathcal{W}$
                \If{$\Omega_{i+1}=\Omega_i$}
                \State\Return $F_\infty = \Omega_i$
                \EndIf
                \EndLoop
            \end{algorithmic}
        \end{whitebox}
    \end{minipage}

    {\footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item Finite $n$ does not always exist, `large' n often good approx.
            \item If $n$ not finite, other methods of computing small invariant sets, slightly larget than $F_\infty$
        \end{itemize}
    }

    \tcbsubtitle{\textbf{(b) -- MODIFY NOMINAL TRAJECTORY CONSTRAINTS}}
    \blue{Noisy System Trajectory}
    \begin{whitebox}{}
        \footnotesize
        Given nominal traj $z_i$ noisy sytem traj $x_i = z_i + e_i$ $\leadsto$ will be smewhr in $\mathcal{E}$
        \vspace{-0.5em}
        \begin{align*}
            x_i \in z_i \oplus \mathcal{E} = \{z_i + e \mid e \in \mathcal{E} \}
        \end{align*}
    \end{whitebox}

    \blue{Goal} $x_i, u_i \in \mathcal{X}, \mathcal{U}$ for all $\{w_i\}\in\mathcal{W}^j$

    \blue{State Condition}
    \begin{highlightbox}{}
        \footnotesize
        Necessary \& Sufficent Condition
        \vspace{-0.5em}
        \begin{align*}
            z_i \oplus \mathcal{E} \subseteq \mathcal{X} 
            \quad \Leftrightarrow \quad 
            z_i \in \mathcal{X} \ominus \mathcal{E}
        \end{align*}
        Set $\mathcal{E}$ known offline -- \textbf{can compute constraints offline!}
    \end{highlightbox}

    \begin{minipage}[c]{0.3\linewidth}
        \blue{Input Condition}
    \end{minipage}
    \begin{minipage}[c]{0.65\linewidth}
        \begin{highlightbox}{}
            \footnotesize
            \vspace*{-1em} 
            \begin{align*}
                u_i \in K\mathcal{E} \oplus v_i \subset \mathcal{U} 
                \quad \Leftrightarrow \quad 
                v_i \in \mathcal{U} \ominus K\mathcal{E}
            \end{align*}
        \end{highlightbox}
    \end{minipage}
\end{whitebox}

\begin{whitebox}{\textbf{(c) -- CONVEX OPTIMIZATION PROBLEM}}
    \blue{Problem Formulation}

    \begin{highlightbox}{}
        \footnotesize
        \vspace{-0.5em}
        \begin{align*}
            &\min_{Z,V} \ l_f(z_N) + \textstyle\sum_{i=1}^{N-1} l(z_i, v_i) \\
            &\left.
            \begin{aligned}
                \mathrm{s.t.}\ &z_{i+1} = Az_i + Bv_i \\
                &z_i \in \mathcal{X} \ominus \mathcal{E}, \quad
                u_i \in \mathcal{U} \ominus K\mathcal{E} \\
                &z_N \in \mathcal{X}_f, \quad
                x(k) \in z_0 \oplus \mathcal{E} \\
            \end{aligned}
            \right\} =: \text{Set } \mathcal{Z}
            \\
            &\mathrm{Control\ Law:}\ \mu_{\mathrm{tube}}(x) := K(x-z_0^\star(x)) + v_0^\star(x)
        \end{align*}
    \end{highlightbox}

    \blue{Remarks}
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Optimizing nominal system with tightened state, input constraints
            \item \textbf{First tube center $z_0$ is opt. var.} $\leadsto$ has to be within $\mathcal{E}$ of $x_0$
            \item Cost is w.r.t tube centers, terminal set is w.r.t tightened constraints
        \end{itemize}
    \end{whitebox}
    {\footnotesize
        \blue{ACHTUNG} $K(x-z_0^\star(x))+v_0^\star(x)$ \textbf{NOT LINEAR} in CL
    }
\end{whitebox}

\begin{whitebox}{\textbf{ROBUST CONSTRAINT SATISFACTION}}
    \blue{Assumptions} almost the same as for nominal MPC
    \begin{whitebox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Stage cost pos def, i.e strictly pos and only 0 at origin
            \item Terminal set invar \textbf{for the nominal sys} under local control law $\kappa_f(z)$: \\
            $\qquad Az + B\kappa_f(z) \in\mathcal{X}_f \quad \forall z\in\mathcal{X}_f$ \\
            All \textbf{tightened state and input constraints} satisfied in $\mathcal{X}_f$: \\
            $\qquad \mathcal{X}_f \subseteq \mathcal{X} \ominus \mathcal{E}, \kappa_f(z)\in\mathcal{U}\ominus K\mathcal{E} \quad \forall z\in\mathcal{X}_f$
            \item Terminal cost is continuous Lyapunov function in terminal set $\mathcal{X}_f$: \\
            $\qquad l_f(Az + B\kappa_f(z)) - l_f(z) \leq -l(z, \kappa_f(z)) \quad \forall z\in\mathcal{X}_f$
        \end{enumerate}
    \end{whitebox}
    \blue{Theorem: Robust Invariance of Tube-MPC}
    \begin{highlightbox}{}
        \footnotesize
        Set $\mathcal{Z}:=\{x\mid \mathcal{Z}\neq \emptyset \}$ 
        is robust invariant set of system $x(k+1) = Ax(k) + B\mu_{\textrm{tube}}(x(k)) + w(k)$ subject to constraints $x,u \in \mathcal{X},\mathcal{U}$
    \end{highlightbox}

    {\footnotesize 
        \blue{Proof} let $(\{v_0^\star\dots v_{N-1}^\star\},\{z_0^\star\dots z_N^\star\})$ be optimal sol'n for $x(k)$
        At next point in time, state $x(k+1)$ may have many possible values due to disturbance \\

        By construction, state $x(k+1)$ in in the set $z_1^\star \oplus \mathcal{E} \ \forall \mathcal{W}$ 

        Therefore the following sequence is feasible for all $x(k+1)$
        \vspace{-0.5em}
        \begin{align*}
            (\{v_1^\star\dots v_{N-1}^\star, \kappa_f(z_N^\star)\},\{\underbrace{z_1^\star\dots z_N^\star}_{\textrm{feas. IC}}, \underbrace{Az_N^\star + B\kappa_f(z_N^\star)}_{\in\mathcal{X}_f \leadsto \textrm{feas.}}\})
        \end{align*}
    }
\end{whitebox}
\begin{whitebox}{\textbf{ROBUST STABILITY}}
    \blue{Robust Stability of Tube-MPC}
    \begin{highlightbox}{}
        State $x(k)$ of system $x(k+1)=Ax(k) + B\mu_{\mathrm{tube}}(x(k)) + w(k)$
        converges in the limit to the set $\mathcal{E}$ 
    \end{highlightbox}
    {\footnotesize
        \blue{Proof} As in standard MPC we have 
        \vspace{-0.5em}
        \begin{align*}
            J^\star(z_0^\star(x(k))) &= l_f(z_N^\star) + \textstyle\sum_{i=0}^{N-1} l(z_i^\star, v_i^\star) \\
            J^\star(z_0^\star(x(k+1))) &\leq l_f(z_N^\star) + \textstyle\sum_{i=1}^{N-1} l(z_i^\star, v_i^\star) \\
                &{\color{color2} + l(z_0^\star, v_0^\star) - l(z_0^\star, v_0^\star) + l_f(z_N^\star) - l_f(z_N^\star)}\\
            = J^\star(x(k)) - &\underbrace{l(z_0^\star, v_0^\star)}_{\geq 0} 
            \underbrace{- l_f(z_N^\star) + l_f(z_{N+1}) + l(z_N^\star, \kappa_f(z_N^\star))}_{\leq 0 \text{ ($l_f$ is lyap function in $\mathcal{X}_f$)}}
        \end{align*}
        This shows $\lim_{k\to\infty}J(z_0^\star(x(k)))=0$, therefore $\lim_{k\to\infty}z_0^\star(x(k))=0$

        \blue{ACHTUNG} 
        \begin{itemize}[leftmargin=1em]
            \item $x(k)$ does \textbf{not tend to 0!} It only stays within robust invar set centered at $z_0^\star(x(k)): lim_{k\to 0} \mathrm{dist}(x(k),\mathcal{E})=0$
            \item Can remove constr. $z_0 \in \mathcal{X} \oplus \mathcal{E}$, doesn't affect recursive stability
            \item $\mathcal{E}$ must be robust positive invariant for proof (so error remains bounded)
        \end{itemize}
    }
\end{whitebox}

\begin{whitebox}{\textbf{TUBE-MPC IMPLEMENTATION}}
    \blue{Offline Design}
    \begin{highlightbox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Choose stabilizing controller $K$ s.t $\lvert\lvert A + BK \rvert\rvert < 1$
            \item Compute mRPI set $\mathcal{E}=F_\infty$ for system $x(k+1) = (A+BK)x(k) + w(k), w \in\mathcal{W}$
            \item Compute tightened constaints $\tilde{\mathcal{X}} := \mathcal{X} \ominus \mathcal{E}, \tilde{\mathcal{U}} := \mathcal{U} \ominus K\mathcal{E}$ 
            \item Choose terminal weight function $l_f$ and constraint $\mathcal{X}_f$ satisfying assumptions on tube MPC (see \textbf{Robust Constraint Satisfaction})
            \begin{itemize}[leftmargin=1em]
                \item Assumption on Terminal set ensures \textbf{Recursive Feasibility}
                \item Assumption on terminal cost ensures \textbf{Asymptotic Stability}
            \end{itemize}
        \end{enumerate}
    \end{highlightbox}

    \blue{LQR Terminal Constraint} (typical choice)
    \begin{whitebox}{}
        \footnotesize
        \begin{itemize}[leftmargin=1em]
            \item Choose LQR terminal control law $\kappa_f(x) = Kx$, ($Q,R$ same as MPC)
            \item Find $\mathcal{X}_f$ invar under this controller s.t satisfies constraints
        \end{itemize}
    \end{whitebox}

    \blue{Online Design}
    \begin{highlightbox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item Measure / Estimate state $x$
            \item Solve optimization problem \\
            $(V^\star(x_0),Z^\star(x_0)) = \mathop{\mathrm{argmin}}_{V,Z}\{J(Z,V) \mid (Z,V) \in \mathcal{Z}(x_0)\}$
            \item Set input to $u=K(x-z_0^\star(x)) + v_0^\star(x)$
        \end{enumerate}
    \end{highlightbox}

    \begin{minipage}[c]{0.50\linewidth}
        \begin{whitebox}{}
            \scriptsize
            \blue{Benefits}
            \begin{itemize}[leftmargin=1em]
                \item Less conservative than OL robust MPC (now actively compensating for noise in prediction)
                \item Works for unstable systems 
                \item Optimization problem to solve is `simple'
            \end{itemize}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \begin{whitebox}{}
            \scriptsize
            \blue{Cons}
            \begin{itemize}[leftmargin=1em]
                \item Sub-optimal MPC (optimal extremely difficult)
                \item Reduced feasible set when compared to nominal MPC 
                \item We need to know what $\mathcal{W}$ is (usually not realistic)
            \end{itemize}
        \end{whitebox}
    \end{minipage}

\end{whitebox}

\begin{whitebox}{\textbf{ROBUST MPC FOR UNCERTAIN SYSTEMS -- SUMMARY}}
    \begin{minipage}[b]{0.50\linewidth}
        \scriptsize
        \blue{Idea} compensate for noise in prediction to ensure constraint satisfaction
        \begin{whitebox}{}
            \scriptsize
            \blue{Benefits}
            \begin{itemize}[leftmargin=1em]
                \item Feasible set invariant -- know exactly when controller will work 
                \item Easier to tune -- knobs to tradeoff robustness against performance
            \end{itemize}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[b]{0.48\linewidth}
        \begin{whitebox}{}
            \scriptsize
            \blue{Cons}
            \begin{itemize}[leftmargin=1em]
                \item Complex (tubes easy to implement, complex to understand)
                \item Must know largest noise $\mathcal{W}$
                \item Often conservative 
                \item Feas set may be small
            \end{itemize}
        \end{whitebox}
    \end{minipage}
\end{whitebox}