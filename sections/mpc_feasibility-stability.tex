\section{FEASIBILITY AND STABILITY}

\begin{whitebox}{\textbf{LQR MPC COMPARISON}}
    \begin{minipage}[c]{0.50\linewidth}
        \blue{LQR}
    \end{minipage}
    \begin{minipage}[c]{0.45\linewidth}
        \blue{MPC}
    \end{minipage}
    \mathboxplus{
        \scriptsize
        \begin{aligned}
            J_\infty^\star(x(k)) &= \min \sum_{i=0}^\infty 
            x_i^\top Q x_i + u_i^\top R u_i \\
            \mathrm{subj.\ to }\ &x_{i+1} = Ax_i + Bu_i \\
            &x_0 = x(k)
        \end{aligned}
        \ \left\lvert \
        \begin{aligned}
            J^\star(x(k)) &= \min \sum_{i=0}^{N-1} 
            x_i^\top Q x_i + u_i^\top R u_i \\
            \mathrm{subj.\ to }\ &x_{i+1} = Ax_i + Bu_i \\
            &x_i \in \mathcal{X}, \quad u_i \in \mathcal{U} \\
            &x_0 = x(k)
        \end{aligned}
        \right.
    }

    {\footnotesize
        \textbf{\textbullet \ Quad. Cost \ \textbullet \ Linear System Dynamics \ \textbullet \ Linear Constraints on $u,x$} \\
        \textbf{Assume:} $Q=Q^\top \succeq 0, R=R^\top \succ 0$
    }
\end{whitebox}
\begin{whitebox}{\textbf{LOSS OF FEASIBILITY \& STABILITY}}
    {\footnotesize
        \blue{Infinite-Horizon} Solve RHC for $N=\infty$, OL traj. are same as CL traj.
        \begin{itemize}[leftmargin=1em]
            \item If problem feasible, CL trajectories always feasible 
            \item If cost finite, states and inputs will converge asympt. to origin
        \end{itemize}
        \blue{Finite-Horizon}
        RHC ``short-sighted'' approximating $\infty$-horizon controller
        \begin{itemize}[leftmargin=1em]
            \item \textbf{Feasibility} -- after some steps finite horizon optimal control problem may become infeasible (disturbances, model mismatch)
            \item \textbf{Stability} -- generated inputs may not lead to traj. that converge to orgin
        \end{itemize}
    }
    \blue{Solution}
    \begin{highlightbox}{}
        \footnotesize
        Introduce terminal cost \& constraints to explicitly ensure feas. \& stab.
        \begin{align*}
            \left.
            \begin{aligned}
                J^\star(x(k)) = &\textstyle\min_U l_f(x_N) + \sum_{i=1}^{N-1} l(x_i, u_i) \\
                \mathrm{subj.\ to }\ & x_{i+1} = Ax_i + Bu_i 
            \end{aligned}
            \ \right\rvert \
            \begin{aligned}
                &x_i\in\mathcal{X}, \quad  u_i \in \mathcal{U} \\
                &x_N \in \mathcal{X}_f, \quad x_0 = x(k)
            \end{aligned}
        \end{align*}
        $l_f(\cdot), \mathcal{X}_f$ chosen to mimic infinite horizon
    \end{highlightbox}
\end{whitebox}
\begin{whitebox}{\textbf{LYAPUNOV STABILITY}}
    \blue{System} NL, TI, DT $x(k+1) = g(x(k))$

    \blue{Asymptotic stability} eq point $\bar{x} \in \Omega$ ($g(\bar{x}) = \bar{x}$)
    \begin{whitebox}{}
        \begin{description}
            \footnotesize
            \item[Asympt. Stable] in pos invar set $\Omega \subseteq \mathbb{R}^n$ if Lyap. stable and attactive 
            \begin{align*}
                \lim_{k\to\infty} \lvert\lvert x(k) - \bar{x} \rvert\rvert = 0 \quad \forall x(0) \in \Omega
            \end{align*}
            \item[Globally Asympt. Stable] if asympt. stable \& $\Omega = \mathbb{R}^n$
        \end{description}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{FEASIBILITY \& STABILITY GUARANTEES OF MPC}}
    \blue{Proof Strategy} 
    \begin{whitebox}{}
        \footnotesize
        \begin{description}
            \item[Recursive Feasibility] show existence of feasible control 
            sequence for all time when starting from feasible initial point 
            \begin{itemize}[leftmargin=1em]
                \item Assume feas. of $x(k)$, $\{u_0^\star, \dots, u_{N-1}^\star \}$, 
                $\{x_0^\star, \dots, x_N^\star \}$ 
                \item At $x(k+1) \Rightarrow \{u_{1}^\star, \dots, \kappa_f(x_N^\star) \}$ should be feas.
            \end{itemize}
            \item[Stability] show that optimal cost is lyap function 
            \begin{itemize}[leftmargin=1em]
                \item $l_f$ necessary to provide cost decrease for asympt. stability
            \end{itemize}
        \end{description}
    \end{whitebox}

    \blue{General Terminal Set $\boldsymbol{\mathcal{X}_f}$}
    \begin{highlightbox}{}
        \textbf{Assumptions}
        {\footnotesize
            \begin{enumerate}[leftmargin=1.5em]
                \item Stage cost pos def, strictly positive, only 0 at origin
                \item Terminal set invariant under local control law \\
                All state and input constraints satisfied in $\mathcal{X}_f$
                \item Terminal cost is cont. Lyap. func. in terminal set $\mathcal{X}_f$ and satisfies
                \begin{align*}
                    l_f(x_{i+1}) - l_f(x_i) \leq - l(x_i, \kappa_f(x_i)) \quad \forall x_i \in \mathcal{X}_f
                \end{align*}
            \end{enumerate}
        }
        \textbf{Theorem} -- CL system under MPC control law $u_0^\star(x)$ asympt. stable and set $\mathcal{X}_N$ is positive invariant for system 
        $x(k+1) = Ax(k) + Bu_0^\star(x(k))$
    \end{highlightbox}
    \blue{Terminal Constraint At Zero $\boldsymbol{x_N \in \mathcal{X}_f=0}$} \\
    $\leadsto$ need large $N$ to approx. max. cont. invar. set 

    \blue{Terminal Set \& Cost -- LQR}
    \begin{whitebox}{}
        \footnotesize 
        \vspace{-1em}
        \begin{align*}
            J^\star(x(k)) = \textstyle\min_U x_N^\top P x_N + \sum_{i=0}^{N-1} x_i^\top Q x_i + u_i^\top R u_i
        \end{align*}
        \begin{itemize}[leftmargin=1em]
            \item Choose $P = P_\infty$ from (D)ARE 
            \item Choose $\mathcal{X}_f$ to be max. invar. set for CL system $(A+BF_\infty)x_k$ \\
            $\leadsto$ ellipsoidal inv. set with Lyap.
            \item All $x,u$ constraints satisfied in $\mathcal{X}_f$
        \end{itemize}
        \textbf{All assumptions of Feasibility \& Stability Theorem Satisfied}
    \end{whitebox}
    \blue{Useful Properties}
    \begin{whitebox}{}
        \footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item $X_1, X_2$ convex invar. for $Ax(k)$ $\leadsto \alpha X_1 \oplus (1-\alpha)X_2$ invar $\forall \alpha \in [0,1]$
            \item $X_1 \subseteq \mathcal{X}, X_2 \subseteq \mathcal{X}$, $X_i,\mathcal{X}$ convex $\leadsto \alpha X_1 \oplus (1-\alpha)X_2 \subseteq \mathcal{X} \ \forall \alpha \in[0,1]$
            \item $V_i(x(k)) = x^\top(k) P_i x(k)$ lyap. func. for $x(k+1)=Ax(k)$, rate of decrease 
            $x^\top(k) \Gamma x(k)$ $\leadsto V(x(k)) = \alpha V_1(x(k)) + (1-\alpha) V_2(x(k))$ 
            also lyap. func. with rate of decrease $x^\top(k) \Gamma x(k)$ for all $\alpha\in[0,1]$
        \end{itemize}
    \end{whitebox}
\end{whitebox}

\begin{whitebox}{\textbf{FEASIBILITY \& STABILITY REMARKS}}
    \begin{itemize}[leftmargin=1em]
        \footnotesize
        \item Terminal constraint provides a \textbf{Suffiecient Condition} for feas. \& stab.
        \item Region of attraction w/o term. const. may be larger than with term. const.
        \item In practice: enlarge horizon and check stability by sampling. As $N\uparrow$, region of attraction appraoches max. control invariant set
        \item CL traj. may not follow assumptions made for OL predictions 
        \item $\infty$-Horizon LQR controller locally optimal $\leadsto$ best choice for quad. cost
        \item \textbf{$\boldsymbol{\infty}$-Horizon provices stab. and invariance. Finite-Horizon MPC may not be stable \& may not satisfy constraints $\forall$ time}
    \end{itemize}
    \blue{Extension to Nonlinearity}
    \begin{itemize}[leftmargin=1em]
        \footnotesize
        \item Assumptions on terminal set/cost did not rely on linearity
        \item Lyapunov stability is general framework (works for NL sys)
        \item \textbf{Results can be directly extended to NL systems}
        \item \textbf{However, computing sets $\mathcal{X}_f$ and function $l_f$ can be very difficult}
    \end{itemize}
\end{whitebox}