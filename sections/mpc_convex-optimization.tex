\section{CONVEX OPTIMIZATION}

\begin{whitebox}{\textbf{PROBLEM FORMULATION}}

    \begin{minipage}[c]{0.50\linewidth}
        \footnotesize
        \mathbox{
            \min_{x\in\mathrm{dom}(f)} &f(x) \\
            \mathrm{subj. \ to \ } & g_i(x) \leq 0 \quad i=1,\dots,m \\
            & h_i(x) = 0 \quad i=1,\dots,p
        }
    \end{minipage}
    \begin{minipage}[c]{0.48\linewidth}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item $\mathcal{X}: \left\{ x\in\mathrm{dom}(f) \mid g_i\leq 0, h_i = 0 \ \right\}$ \textbf{feasible set}
            \item $g_i:$ \textbf{ineq constraints}
            \item $h_i:$ \textbf{eq contraints}
        \end{itemize}
    \end{minipage}

    \vspace{0.5em}

    \footnotesize
    \begin{minipage}[t]{0.43\linewidth}
        \blue{Feasibility Point} $x$ satisfies $g_i \leq 0, h_i =0$ \& eq contraints \\
        \blue{Optimal Value} lowest cost
        \mathbox{
            p^* = f(x^\star) = \textstyle\min_{x\in\mathcal{X}}f(x)
        }
    \end{minipage}
    \begin{minipage}[t]{0.55\linewidth}
        \blue{Strictly Feasible Point} $x$ satisfies $g_i < 0$ \\
        \blue{Optimizer} feas. $x^\star$ $\leadsto$ smallest $p^*$
        \mathbox{
            \textstyle\mathop{\mathrm{argmin}}_{x\in\mathcal{X}} f(x) \!:=\! \{ x \!\in\! \mathcal{X} | f(x) \!=\! p^*\}
        }
        \blue{ACHTUNG} \textbf{NOT always unique}
    \end{minipage}

    \vspace{0.25em}

    \begin{minipage}[t]{0.55\linewidth}
        \blue{Active Contraints} \\ when ineq const. are eq $\leadsto$ ``active'' \\
        \blue{Locally Optimal}
        \mathboxplus{
            y\in \mathcal{X}, \lvert\lvert y-x \rvert\rvert \leq R \Rightarrow f(y) \geq f(x)
        }
        \blue{Unbounded Below} $p^* = -\infty$  \\
        \blue{Unconstrained} $\mathcal{X} = \mathbb{R}^n$
    \end{minipage}
    \begin{minipage}[t]{0.43\linewidth}
        \blue{Redundant Contraints} do not change feasible set \\
        \blue{Globally Optimal}
        \mathboxplus{
            y\in \mathcal{X} \Rightarrow f(y) \geq f(x)
        }
        \blue{Infeasible} $p^* = \infty \Leftrightarrow \mathcal{X} = \{\}$  \\
    \end{minipage}
\end{whitebox}

\begin{whitebox}{\textbf{CONVEX SETS}}

    \begin{minipage}[c]{0.20\linewidth}
        \blue{Definition} \textbf{Convex iff}
    \end{minipage}
    \begin{minipage}[c]{0.78\linewidth}
        \mathboxplus{
            \lambda x + (1-\lambda)y \in \mathcal{X}, 
            \quad \forall \lambda \in [0,1], \ \forall x,y \in \mathcal{X}
        }
    \end{minipage}

    \vspace{0.25em}
    
    \blue{Interpretation} $\leadsto$ \textbf{All lines starting in $\mathcal{X}$ stay within $\mathcal{X}$} \\
    
    \begin{whitebox}{}
        \footnotesize
        \begin{minipage}[c]{0.35\linewidth}
            \blue{Hyperplane} 
            \mathbox{
                \{ x \!\in\! \mathbb{R}^n \mid a^\top x \!=\! b\}
            }
            \blue{Halfspace}
            \mathbox{
                \{ x \!\in\! \mathbb{R}^n \mid a^\top x \!\leq\! b\}
            }
            open: $<$, closed: $\leq$
        \end{minipage}
        \begin{minipage}[c]{0.60\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {18},
                trim = {1.8cm, 0.6cm, 1.8cm, 5cm},
                clip
                ]{03_optimization.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \begin{whitebox}{}
        \footnotesize
        \begin{minipage}[c]{0.48\linewidth}
            \blue{Polyhedron}
            \mathbox{
                P :=& \{ x \mid a_i^\top x \leq b_i, i=\dots \} \\
                :=& \{ x \mid A x \leq b \}
            }
            \blue{Polytope} \\
            \textbf{bounded} polyhedron 
        \end{minipage}
        \begin{minipage}[c]{0.50\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {19},
                trim = {0.8cm, 0.6cm, 4cm, 5.5cm},
                clip
                ]{03_optimization.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \begin{whitebox}{}
        \footnotesize
        \begin{minipage}[c]{0.48\linewidth}
            \blue{Ellipsoid}
            \mathbox{
                \{ x | (x\!-\!x_c)^\top A^{-1}(x\!-\!x_c) \leq 1 \}
            }
            $x_c:$ center of ellipsoid
        \end{minipage}
        \begin{minipage}[c]{0.50\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {20},
                trim = {2cm, 2.2cm, 2cm, 4.4cm},
                clip
                ]{03_optimization.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \begin{whitebox}{}
        \footnotesize
        \begin{minipage}[c]{0.60\linewidth}
            \begin{minipage}[c]{0.25\linewidth}
                \blue{Norm Ball}
            \end{minipage}
            \begin{minipage}[c]{0.70\linewidth}
                \mathbox{
                    \{ x \mid \lvert\lvert x\!-\!x_c \rvert\rvert \leq r \}
                }
            \end{minipage}

            \vspace{0.25em}
            
            \begin{itemize}[leftmargin=1em]
                \item $p=2$ Euclidean Norm 
                $\lvert\lvert x \rvert\rvert_2 = \sqrt{\sum_i x_i^2}$
                \item $p=1$ Sum of Absolute 
                $\lvert\lvert x \rvert\rvert_1 = \sum_i \lvert x_i \rvert$
                \item $p=\infty$ Largest Absolute 
            \end{itemize}
        \end{minipage}
        \begin{minipage}[c]{0.38\linewidth}
            \resizebox{\linewidth}{!}{
                \includegraphics[
                page = {21},
                trim = {6.4cm, 3.4cm, 0.4cm, 3.2cm},
                clip
                ]{03_optimization.pdf}
            }
        \end{minipage}
    \end{whitebox}

    \blue{Intersections \& Unions}
    \begin{highlightbox}{}
        \footnotesize
        \textbf{Intersection} -- Intersection of two or more convex sets is itself convex \\
        \textbf{Union} -- Union of two sets is \textbf{NOT} convex in general
    \end{highlightbox}
    
\end{whitebox}

\begin{whitebox}{\textbf{CONVEX FUNCTIONS}}

    \begin{minipage}[c]{0.68\linewidth}
        \blue{Definition} \textbf{convex iff} $\mathrm{dom}(f)$ convex \&
        \mathboxplus{
            &f(\lambda x \!+\! (1 \!-\! \lambda)y) \leq \lambda f(x) \!+\! (1 \!-\! \lambda) f(y) \\
            &\forall \lambda \in (0,1), \quad \forall x,y \in \mathrm{dom}(f)
        }
        \blue{Strictly Convex} if inequality is strict
    \end{minipage}
    \begin{minipage}[c]{0.30\linewidth}
        \resizebox{\linewidth}{!}{
            \includegraphics[
            page = {25},
            trim = {4cm, 1.5cm, 4cm, 4cm},
            clip
            ]{03_optimization.pdf}
        }
    \end{minipage}
    
    \begin{minipage}[c]{0.40\linewidth}
        \blue{1st-Order Condition} \\
        $f(x)$ convex \textbf{iff}
    \end{minipage}
    \begin{minipage}[c]{0.58\linewidth}
        \mathbox{
            f(y) \geq f(x) + \nabla f(x)^\top (y-x)
        }
    \end{minipage}

    \begin{minipage}[c]{0.40\linewidth}
        \blue{2nd-Order Condition} \\
        $f(x)$ convex \textbf{iff}
    \end{minipage}
    \begin{minipage}[c]{0.58\linewidth}
        \mathbox{
            \nabla^2 f(x) \succeq 0, 
            \quad \nabla^2 f(x)_{ij} = \textstyle\frac{\partial^2 f(x)}{\partial x_i \partial x_j}
        }
    \end{minipage}

    \vspace{0.25em}

    \begin{minipage}[c]{0.48\linewidth}
        \blue{Level Set}
        \begin{whitebox}{}
            \footnotesize
            $L_\alpha$ of $f$, set for which 
            \begin{align*}
                L_\alpha := \{x \mid x \!\in\! \mathrm{dom}(f), f(x)\!=\!\alpha \}
            \end{align*}
            \scriptsize
            Equiv to contour lines of const `height'
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[c]{0.50\linewidth}
        \blue{Sublevel Set}
        \begin{whitebox}{}
            \footnotesize
            $C_\alpha$ of $f$ for value $\alpha$ is defined by 
            \begin{align*}
                C_\alpha := \{x \mid x\in\mathrm{dom}(f), f(x) \leq \alpha\}
            \end{align*}
            \scriptsize
            $f$ convex $\Rightarrow$ sublevel sets convex $\forall\alpha$
        \end{whitebox}
    \end{minipage}
    
    \vspace{0.25em}
    
    \blue{Examples}

    \begin{minipage}[c]{0.53\linewidth}
        \begin{whitebox}{}
            \scriptsize
            \textbf{Convex}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{Affine} $ax + b$ for any $a,b\in\mathbb{R}$
                \item \textbf{Exp.} $e^{ax}$ for any $A\in\mathbb{R}$
                \item \textbf{Powers} $x^\alpha$ on domain $\mathbb{R}_{++}$, for $\alpha \geq 1$ or $\alpha \leq 0$
                \item \textbf{Vector norms} on $\mathbb{R}^n$: \\
                $\left\|x\right\|_p = (\sum_{i=1}^n |x|^p)^{1/p}$, for $p\geq1$, 
            \end{itemize}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[c]{0.45\linewidth}
        \begin{whitebox}{}
            \scriptsize
            \textbf{Concave}
            \begin{itemize}[leftmargin=1em]
                \item \textbf{Affine} $ax + b$ for any $a,b\in\mathbb{R}$
                \item \textbf{Powers} $x^\alpha$ on domain $\mathbb{R}_{++}$, for $0 \leq \alpha \leq 1$
                \item \textbf{Log} $\log x$ on domain $\mathbb{R}_{++}$
                \item \textbf{Entropy} $-x\log x$ on domain $\mathbb{R}_{++}$
            \end{itemize}
        \end{whitebox}
    \end{minipage}


\end{whitebox}

\begin{whitebox}{\textbf{OPTIMALITY CONDITIONS}}

    \blue{Lagrange Dual Function}
    \mathboxplus{
        d(\lambda, \nu) = \underbrace{\inf_{x\in\mathrm{dom}(f)}}_{\approx \min_x}
        \underbrace{\left[ f(x) + \textstyle\sum_{i=1}^{m}\lambda_i g_i(x) 
        + \textstyle\sum_{i=1}^{p} \nu_i h_i(x) \right]}_{L(x,\lambda,\nu): \text{ Lagrange Function}}
    }

    \begin{minipage}[c]{0.50\linewidth}
        \blue{P -- Primal Problem}
    \end{minipage}
    \begin{minipage}[c]{0.45\linewidth}
        \blue{D -- Dual Problem}
    \end{minipage}
    \mathboxplus{
        (P):\
        \left.
        \begin{aligned}
            \textstyle\min_{x} \ &f(x) \\
            \mathrm{subj. \ to \ } &g_i(x) \leq 0 \\
            &h_i(x) = 0
        \end{aligned}
        \quad
        \right\rvert
        \quad
        (D):\
        \begin{aligned}
            \textstyle\max_{\nu, \lambda} \ &d(\nu, \lambda) \\
            \mathrm{subj. \ to \ } &\lambda \geq 0
        \end{aligned}
    }

    \begin{minipage}[c]{0.45\linewidth}
        \footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item $d(\lambda, \nu)$ always \textbf{concave}
            \item $d^\star \leq p^\star$ $\leadsto$ $d(\lambda,\nu)$ gens lower bound for $p$
        \end{itemize}
    \end{minipage}
    \begin{minipage}[c]{0.50\linewidth}
        \footnotesize 
        \begin{itemize}[leftmargin=1em]
            \item $(D)$ \textbf{convex} even if $(P)$ not
            \item Point $(\lambda,\nu)$ \textbf{dual feas.} if $\lambda \geq 0$, $(\lambda, \nu) \in \mathrm{dom}(d)$
        \end{itemize}
    \end{minipage}

    \vspace{0.25em}

    \blue{Weak \& Strong Duality}
    \begin{whitebox}{}
        \footnotesize
        \textbf{Weak Duality} -- it is \textbf{always} true that $d^\star \leq p^\star$ 

        \textbf{Stront Duality} -- it is \textbf{sometimes} true that $d^\star = p^\star$
        \begin{itemize}[leftmargin=1em]
            \item Strong duality usually does not hold for non-convex problems
            \item Can impose conditions on convex problems to guarantee that $d^\star=p^\star$
            \item Sometimes the dual much easier to solve than the primal
        \end{itemize}
    \end{whitebox}
    {\footnotesize
        \begin{itemize}
            \item \textbf{LP always has strong duality}
        \end{itemize}
    }
    \blue{Slater Condition}
    \begin{highlightbox}{}
        \footnotesize
        If $\exists$ at least one \textbf{strictly feasible point}  \emph{i.e} $\{x \mid Ax=b, g_i(x)<0 \ \forall i\}$ $\ \Rightarrow \ p^\star = d^\star$
    \end{highlightbox}

\end{whitebox}

\begin{whitebox}{\textbf{KKT -- KARUSH-KUHN-TUCKER CONDITIONS}}
    \begin{highlightbox}{}
        \footnotesize
        \begin{enumerate}[leftmargin=1.75em, label=\textbf{(\arabic*)}]
            \item \textbf{Primal Feasibility} $g_i(x^\star) \leq 0, \ i=1\dots m \qquad h_i(x^\star) = 0, \ i=1\dots p$
            \item \textbf{Dual feasibility} $\quad \lambda^\star \geq 0$
            \item \textbf{Complementary Slackness} $\quad \lambda_i^\star g_i(x^\star) = 0 \qquad i=1\dots m$
            \item \textbf{Stationarity} $\nabla L = \nabla f(x^*) + \displaystyle\sum_{i=1}^{m}\lambda_i^* \nabla g_i(x^*) 
            + \displaystyle\sum_{i=1}^{p} \nu_i^* \nabla h_i(x^*) = 0$
        \end{enumerate}
    \end{highlightbox}

    \blue{General Optimization} \textbf{Necessary condition}
    \begin{whitebox}{}
        \footnotesize
        $x^\star, \lambda^\star,\nu^\star$ sol'n to $(P)$, $(D)$ with 0 duality gap
        $\Rightarrow$ $x^\star, \lambda^\star,\nu^\star$ satisfy KKT
    \end{whitebox}

    \blue{Convex Optimization} \textbf{Sufficient condition}
    \begin{whitebox}{}
        \footnotesize
        $x^\star, \lambda^\star,\nu^\star$ satisfy KKT
        $\Rightarrow$ $x^\star, \lambda^\star,\nu^\star$ sol'n to $(P)$, $(D)$ with 0 duality gap
    \end{whitebox}

    \blue{Convex Opt. + Slater} \textbf{Necessary \& Sufficient condition}
    \begin{whitebox}{}
        \footnotesize
        If Slater's cond. holds, $x^\star, \lambda^\star,\nu^\star$ are sol'n to $(P)$, $(D)$
        \textbf{\underline{IFF}} KKT satisfied
    \end{whitebox}
    {\footnotesize
        \blue{Remark} for convex opt. problem, KKT conditions sufficent $\leadsto$ if $x^\star, \lambda^\star,\nu^\star$ satisfy KKT
        then $p^\star = d^\star$
    }
\end{whitebox}

\begin{whitebox}{\textbf{MATRIX CALCULUS}}
    \footnotesize

    \begin{minipage}[t]{0.50\linewidth}
        \blue{Basics}
        \begin{whitebox}{}
            \vspace{-0.5em}
            \begin{align*}
                xy^\top &= 
                    \begin{bmatrix}
                        x_1 y & \dots & x_n y
                    \end{bmatrix}
                    \\
                    \langle x,y \rangle &= x^\top y = \textstyle\sum x_i y_i
            \end{align*}
        \end{whitebox}
    \end{minipage}
    \begin{minipage}[t]{0.48\linewidth}
        \blue{Vector Derivatives}
        \begin{highlightbox}{}
            \vspace{-0.25em}
            \begin{align*}
                \textstyle\frac{\partial}{\partial x} x^\top A &= \textstyle\frac{\partial}{\partial x} A^\top x = A \\
                \textstyle\frac{\partial}{\partial x} x^\top A x &= (A + A^\top)x
            \end{align*}
        \end{highlightbox}
    \end{minipage}

    \vspace{0.25em}
    
    \begin{minipage}[c]{0.2\linewidth}
        \blue{Del-Operator} \\
        (Gradient)
    \end{minipage}
    \begin{minipage}[c]{0.78\linewidth}
        \begin{whitebox}{}
            \vspace{-0.5em}
            \begin{align*}
                \nabla_x f(x) = 
                \begin{bmatrix}
                    \frac{\partial}{\partial x_1} f(x) &
                    \cdots &
                    \frac{\partial}{\partial x_n} f(x)
                \end{bmatrix}^\top
            \end{align*}
        \end{whitebox}
    \end{minipage}

    \begin{minipage}[c]{0.2\linewidth}
        \blue{Jacobian} \\
        (Gradient of multivar func)
    \end{minipage}
    \begin{minipage}[c]{0.78\linewidth}
        \begin{whitebox}{}
            \vspace{-0.5em}
            \begin{align*}
                \frac{\partial f}{\partial x^\top} = 
                \begin{bmatrix}
                    \frac{\partial f_1}{\partial x_1} & \dots & \frac{\partial f_1}{\partial x_n} \\
                    \frac{\partial f_n}{\partial x_1} & \dots & \frac{\partial f_n}{\partial x_n}
                \end{bmatrix}
            \end{align*}
        \end{whitebox}
    \end{minipage}

    

    \tcbsubtitle{\textbf{EXAMPLES}}

    \blue{LP -- Dual}
    \begin{whitebox}{}
        \vspace{-0.5em}
        \begin{align*}
            \begin{array}{c r l}
                \mathrm{(P):}&
                \textstyle\min_{x\in\mathbb{R}^n} \ &c^\top x, \quad \mathrm{subj.\ to}\ Ax=b, \quad Cx\leq e \\
                \midrule
                \mathrm{(D):}&
                \textstyle\max_{\lambda,\nu} &-b^\top \nu -e^\top \lambda, \quad
                \mathrm{s.t}\ A^\top \nu + C^\top \lambda + c = 0, \quad \lambda \geq 0
            \end{array}
        \end{align*}
    \end{whitebox}

    \blue{QP -- Dual} with $Q \succ 0$
    \begin{whitebox}{}
        \vspace{-0.5em}
        \begin{align*}
            \begin{array}{c r l}
                \mathrm{(P):}&
                \textstyle\min_{x\in\mathbb{R}^n} &\textstyle\frac{1}{2}x^\top Q x + c^\top x, \quad
                \mathrm{subj.\ to}\ Cx\leq e \\
                \midrule
                \mathrm{(D):}&
                \textstyle\max_{\lambda,\nu} &\textstyle\frac{1}{2} \lambda^\top C Q^{-1}C^\top \lambda + (CQ^{-1}c + e)^\top\lambda + \frac{1}{2}c^\top Q^{-1}c \\
                &\mathrm{subj.\ to}\ &\lambda \geq 0
            \end{array}
        \end{align*}
    \end{whitebox}

    \blue{QP -- Lagrangian}
    \begin{whitebox}{}
        \vspace{-0.5em}
        \begin{align*}
            \left.
            \begin{aligned}
                \min_{x} \ &\textstyle\frac{1}{2} x^\top H x + q^\top x + r \\
                \mathrm{s.t}\ &Gx \leq h \\
                & Ax = b
            \end{aligned}
            \quad \right\rvert \quad
            \begin{aligned}
                L =& \textstyle\frac{1}{2} x^\top H x + q^\top x + r \\
                &+ \lambda^\top (Gx-h) + \nu^\top (Ax - b) \\
                \nabla_x L =& Hx + q + G^\top \lambda + A^\top \nu
            \end{aligned}
        \end{align*}
    \end{whitebox}
\end{whitebox}