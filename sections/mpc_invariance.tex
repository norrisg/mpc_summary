\section{INVARIANCE}

\begin{whitebox}{\textbf{INVARIANCE}}
    \blue{System} 
    \begin{description}
        \item[Autonomous] $x(k+1) = g(x(k))$
        \item[Closed-Loop] $x(k+1) = g(x(k), \kappa(x(k)))$ for given $\kappa$
    \end{description}

    \blue{Positively Invariant Set}
    \begin{highlightbox}{}
        Set $\mathcal{O}$ positively invariant
        for autonomous system if
        \begin{align*}
            x(k) \in \mathcal{O} \Rightarrow x(k+1) \in \mathcal{O}, \quad \forall k \in \{0,1,\dots\}
        \end{align*}
    \end{highlightbox}
    \blue{Maximal Positively Invariant Set}
    \begin{highlightbox}{}
        $\mathcal{O}_\infty \subset \mathcal{X}$ \
        positively invariant and contains all other $\mathcal{O}$
    \end{highlightbox}
    \blue{Pre-Set}
    \begin{highlightbox}{}
        Given set $S$, the pre-set of $S$ is the set of states that evolve into $S$ in one time step
        \begin{align*}
            \left.
            \begin{aligned}
                &x(k+1) = g(x(k)) \\
                \Rightarrow &\mathrm{pre}(S) := \{x\mid g(x)\in S\} \\
            \end{aligned}
            \ \right\rvert \
            \begin{aligned}
                &x(k+1) = Ax(k) \\
                \Rightarrow &\mathrm{pre}(S) := \{ x \mid Ax \in S \}
            \end{aligned}
        \end{align*}
    \end{highlightbox}

    \blue{Invariant Set Conditions}
    \begin{highlightbox}{}
        Set $\mathcal{O}$ is positively invariant set iff 
        \begin{align*}
            \mathcal{O} \subseteq \mathrm{pre}(\mathcal{O}) 
            \ \Leftrightarrow \
            \mathrm{pre}(\mathcal{O}) \cap \mathcal{O} = \mathcal{O}
        \end{align*}
    \end{highlightbox}
    \begin{description}
        \footnotesize
        \item[Necessary] if $\mathcal{O} \nsubseteq  \mathrm{pre}(\mathcal{O})$, then $\exists\bar{x} \in \mathcal{O}$ s.t $\bar{x} \notin \mathrm{pre}(\mathcal{O})$ 
        $\leadsto \bar{x}\in\mathcal{O}, \bar{x}\notin\mathrm{pre}(\mathcal{O})$, thus $\mathcal{O}$ not positively invariant 
        \item[Sufficient] if $\mathcal{O}$ not pos invar set, then $\exists \bar{x}\in\mathcal{O}$ s.t $g(\bar{x}) \notin\mathcal{O}$ 
        $\leadsto \bar{x}\in\mathcal{O}, \bar{x}\notin\mathrm{pre}(\mathcal{O})$ thus $\mathcal{O}\notin \mathrm{pre}(\mathcal{O})$
    \end{description}
    
    \begin{minipage}[b]{0.48\linewidth}
        \blue{Pre-Set Computation}
        \begin{highlightbox}{}
            Set $S := \{x \mid Fx \leq f\}$, $x(k+1) = Ax(k)$ then
            \begin{align*}
                \mathrm{pre}(S) :=& \{x \mid Ax \in S\} \\
                =& \{ x \mid FAx \leq f\}
            \end{align*}
        \end{highlightbox}
    \end{minipage}
    \begin{minipage}[b]{0.50\linewidth}
        \begin{whitebox}{}
            \begin{algorithmic}
                \footnotesize
                \State $\Omega_0 \leftarrow \mathcal{X}$
                \Loop
                \State $\Omega_{i+1} \leftarrow \mathrm{pre}(\Omega_i)\cap\Omega_i$
                \If{$\Omega_{i+1}=\Omega_i$}
                \State\Return $\mathcal{O}_\infty = \Omega_i$
                \EndIf
                \EndLoop
            \end{algorithmic}
        \end{whitebox}
    \end{minipage}

    {\footnotesize
        \begin{itemize}[leftmargin=1em]
        \item For $\{x\mid Fx \leq f\}$, if $F\downarrow$ or  $f\uparrow \ \leadsto$ \textbf{\emph{Less} Restrictive}
        \item $S \cap F \ \leadsto$ constraints from both sets active
        \end{itemize}
    }
    
\end{whitebox}

\begin{whitebox}{\textbf{CONTROL INVARIANCE}}
    \blue{Control Invariant Set}
    \begin{highlightbox}{}
        Set $\mathcal{C} \subseteq \mathcal{X}$ control invariant if 
        \begin{align*}
            x(k) \in \mathcal{C} \quad \Rightarrow \ \exists u(k) \in \mathcal{U} \textrm{ s.t } g(x(k), u(k))\in\mathcal{C} \ \forall k
        \end{align*}
    \end{highlightbox}
    \blue{Maximal Control Invariant Set}
    \begin{highlightbox}{}
        Set $\mathcal{C}_\infty$ maximal control invariant if it is control invariant and contains all control invariant sets contained in $\mathcal{X}$
    \end{highlightbox}
    {\footnotesize
        \blue{Intuition} For all states in $\mathcal{C}_\infty$, there exists control law s.t system constraints never violated 
        $\leadsto$ \textbf{The best any controller could ever do}
    }

    \blue{Pre-Set} $\mathrm{pre}(S) := \{ x \mid \exists u \in \mathcal{U} \textrm{ s.t } g(x,u) \in S\}$ \\
    \blue{Control Invariant} $\mathcal{C}$ control invariant set iff $\mathcal{C} \subseteq \mathrm{pre}(\mathcal{S})$ \\
    \blue{Algorithm} Same, but \textbf{much harder to compute pre-set}

    \blue{Control Invariant Set $\Rightarrow$ Control Law}
    \begin{whitebox}{}
        \footnotesize
        $\mathcal{C}$ control invariant set for $x(k+1)=g(x(k),u(k))$
        Control law $\kappa(x(k))$ will guarantee that system satisfies constraints 
        $\forall t$ if $g(x,\kappa(x)) \in \mathcal{C} \ \forall x \in \mathcal{C}$
        $\leadsto$ With $f$ as any function Synthesize control law $\kappa$:
        \begin{align*}
            \kappa(x) := \mathrm{argmin}\{f(x,u) \mid g(x,u)\in\mathcal{C}\}
        \end{align*}
    \end{whitebox}
    \begin{itemize}[leftmargin=1em]
        \footnotesize
        \item Does not ensure sys. will converge, but will satisfy constraints 
        \item Don't often do because calculating control invariant sets is very hard
        \item \textbf{MPC implicitly} describes cont. invar. set s.t easy to represent/compute
    \end{itemize}
\end{whitebox}

\begin{whitebox}{\textbf{PRACTICAL INVARIANT SET COMPUTATION}}
    \blue{Minkowski-Weyl Theorem}
    \begin{whitebox}{}
        \footnotesize
        For $P\subseteq \mathbb{R}^d$ following statements equivalent:
        \begin{itemize}[leftmargin=1em]
            \item P polytope, $\exists A, b$ s.t $P = \{x \mid Ax \leq b\}$
            \item P finitely generated, $\exists$ finite set of vectors $\{v_i\}$ 
            s.t $P=\mathrm{co}(\{v_1 \dots v_s\})$
        \end{itemize}
    \end{whitebox}
    \blue{Invariant Sets from Lyapunov Functions}
    \begin{highlightbox}{}
        \footnotesize
        \textbf{Lemma} If $V: \mathbb{R}^n \to \mathbb{R}$ a Lyap. func. for sys. $x(k+1) = g(x(k))$, then
        $Y := \{x \mid V(x) \leq \alpha\}$
        is an invariant set for all $\alpha \geq 0$

        \textbf{Proof}
        \begin{itemize}[leftmargin=1em]
            \footnotesize
            \item $V(x) \geq 0 \ \forall x$
            \item $V(g(x)) - V(x) < 0$ $\leadsto$ once $V(x(k))\leq \alpha$, will remain there for all $j\geq k$ $\leadsto$ Invariance
        \end{itemize}

        \textbf{Example System} for $x(k+1) = Ax(k)$ with $P\succ 0$ that satisfies $A^\top P A - P \prec 0$ $\leadsto$ then $V(x(k)) = x(k)^\top P x(k)$ is Lyap. function

        Goal -- find largest $\alpha$ s.t set $Y_\alpha \in \mathcal{X}$ \\
        $\qquad Y_\alpha := \{x \mid x^\top P x \leq \alpha\}\subset \mathcal{X} := \{x \mid Fx\leq f\}$ \\
        Equivalent to $\qquad \max_\alpha \alpha \quad \mathrm{subj.\ to}\ h_{Y_\alpha}(F_i) \leq f_i \ \forall i \in \{1\dots n\}$
    \end{highlightbox}

    \blue{Maximum Ellipsoidal Invariant Sets}
    \begin{whitebox}{}
        \footnotesize
        \textbf{Support of an ellipse:} $ h_{Y_\alpha}(F_i) = \max_x F_i x \quad \mathrm{subj.\ to }\ x^\top P x \leq \alpha $

        \textbf{Change of Variables:} $y := P^{1/2}x$ \\
        $\qquad \leadsto h_{Y_\alpha}(F_i) = \max_x F_i P^{-1/2}y \quad \mathrm{s.t}\ y^\top y \leq \sqrt{\alpha}^2$

        \textbf{Maximizer found by inspection:}
        \begin{align*}
            h_{Y_\alpha}(F_i) = F_i P^{-1/2} \textstyle\frac{P^{-1/2}F_i^\top}{\lvert\lvert P^{-1/2}F_i^\top \rvert\rvert}
            \sqrt{\alpha} = \lvert\lvert P^{-1/2}F_i^\top \rvert\rvert\sqrt{\alpha}
        \end{align*}
        \textbf{Largest ellipse now 1-dim optimization problem:}
        \begin{align*}
            \alpha^\star &= \textstyle\max_\alpha \alpha \quad \textrm{s.t. } \lvert\lvert P^{-1/2}F_i^\top\rvert\rvert^2\alpha \leq f_i^2 \ \forall i \in\{1\dots n\} \\
            &= \textstyle\min_{i\in\{1\dots n\}} \frac{f_i^2}{F_i P^{-1}F_i^\top}
        \end{align*}
    \end{whitebox}
\end{whitebox}